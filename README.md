# INF112 Project - Battle Against the Cosmic Crusaders

- Team: Cosmic Crusade Studios (Gruppe 3) *Jonas Lund Qvigstad, Daniel Rolfsnes, Magnus Severinsen og Benjamin Ramslien*
- Link: https://git.app.uib.no/cosmic-crusade-studios/battle-against-the-cosmic-crusaders
- https://trello.com/b/gcUEuXy8/battle-of-the-cosmic-crusaders

## Om spillet:

* Som spiller av dette spillet har du muligheten til å bruke romskipet ditt til å reise til forskjellige planeter. På
  disse planetene kan du møte forskjellige personer og skapninger på veien. Kanskje du møter på veikryss og items også. I tillegg
  til å utforske de forskjellige planetene har du også ett helt skip med flere rom du kan utforske. 

## Kjøring

- Kompileres med `mvn package`
- kjøres med `java -jar target/gdx-app-1.0-SNAPSHOT-fat.jar`
- Trenger JDK 18 eller nyere versjon

## Testing

- For å kjøre testene må man kjøre AppTest, PlayerTests og NPCTests. Rekke følgen er ikke viktig.

## Kjente feil

- Visuelle "rifts"
- På noen maskiner beveger spilleren seg tregere enn på andre maskiner.
- Spilleren henger seg fast i taket om den holder inne "Space"
- Vi har tenkt å utvikle spillet videre på fritiden, så noe av info under help er feil (som at geiten er passiv, og at vi kan møte visse NPCer)

## Credits

The Creative Commons Zero v1.0 license applies only to the graphics and sound included in this project, and not to any other part of the project.<br>
Grafikk - Daniel Rolfsnes<br>
Lyd - Daniel Rolfsnes

## Roller

- Daniel - Lyd, Grafikk
- Jonas - Programmeringsansvarlig & Prosjektleder
- Benjamin - Leveldesign
- Magnus - Testansvarlig
