<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.10" tiledversion="1.10.0" name="WallTiles" tilewidth="16" tileheight="16" tilecount="550" columns="22">
 <image source="WallTiles.png" width="352" height="400"/>
 <tile id="101">
  <objectgroup draworder="index" id="2">
   <object id="1" x="12" y="4">
    <polyline points="0,12 0,12"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="103">
  <objectgroup draworder="index" id="2">
   <object id="1" x="4" y="12">
    <polyline points="0,4 0,4"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="104">
  <objectgroup draworder="index" id="2">
   <object id="1" x="12" y="16">
    <polyline points="0,0 -0.000526316,-0.407895"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="107">
  <objectgroup draworder="index" id="2">
   <object id="1" x="4" y="16">
    <polyline points="0,0 -0.118421,-0.934211"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="123">
  <objectgroup draworder="index" id="2">
   <object id="1" x="12" y="0">
    <polyline points="0,0 0,16"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="125">
  <objectgroup draworder="index" id="2">
   <object id="1" x="4" y="0">
    <polyline points="0,0 0,16"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="126">
  <objectgroup draworder="index" id="2">
   <object id="1" x="12" y="0">
    <polyline points="0,0 0,16"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="129">
  <objectgroup draworder="index" id="2">
   <object id="1" x="4" y="0">
    <polyline points="0,0 0,16"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="145">
  <objectgroup draworder="index" id="2">
   <object id="1" x="12" y="0">
    <polyline points="0,0 0,16"/>
   </object>
   <object id="2" x="12" y="12">
    <polyline points="0,0 -12,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="147">
  <objectgroup draworder="index" id="2">
   <object id="1" x="4" y="0">
    <polyline points="0,0 0,16"/>
   </object>
   <object id="2" x="0" y="12">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="148">
  <objectgroup draworder="index" id="2">
   <object id="1" x="12" y="0">
    <polyline points="0,0 0,16"/>
   </object>
   <object id="2" x="16" y="12">
    <polyline points="0,0 -16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="151">
  <objectgroup draworder="index" id="2">
   <object id="1" x="4" y="0">
    <polyline points="0,0 0,16"/>
   </object>
   <object id="2" x="0" y="12">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="187">
  <objectgroup draworder="index" id="2">
   <object id="1" x="4" y="16">
    <polyline points="0,0 0,-8"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="189">
  <objectgroup draworder="index" id="2">
   <object id="1" x="12" y="16">
    <polyline points="0,0 0,-8"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="209">
  <objectgroup draworder="index" id="2">
   <object id="1" x="4" y="8">
    <polyline points="0,-8 0,-4 12,-4"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="210">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="8">
    <polyline points="0,-4 16,-4"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="211">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="8">
    <polyline points="0,-4 12,-4 12,-8"/>
   </object>
  </objectgroup>
 </tile>
</tileset>
