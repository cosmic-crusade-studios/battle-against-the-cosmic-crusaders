<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.10" tiledversion="1.10.0" name="ObjectTiles" tilewidth="16" tileheight="16" tilecount="936" columns="26">
 <image source="ObjectTiles.png" width="416" height="578"/>
 <tile id="15">
  <objectgroup draworder="index" id="2">
   <object id="1" x="6.25" y="14.5395" width="7.56579" height="1.31579"/>
  </objectgroup>
 </tile>
 <tile id="27">
  <objectgroup draworder="index" id="2">
   <object id="1" x="4" y="0">
    <polyline points="0,0 8,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="52">
  <objectgroup draworder="index" id="2">
   <object id="1" x="4" y="16">
    <polyline points="0,0 0,-8 12,-8"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="53">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="8">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="54">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="8">
    <polyline points="0,0 12,0 12,8"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="55">
  <objectgroup draworder="index" id="2">
   <object id="1" x="16" y="8">
    <polyline points="0,0 -12,0 -12,8"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="56">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="8">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="57">
  <objectgroup draworder="index" id="2">
   <object id="1" x="12" y="16">
    <polyline points="0,0 0,-8 -12,-8"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="58">
  <objectgroup draworder="index" id="2">
   <object id="1" x="4" y="16">
    <polyline points="0,0 0,-8 8,-8 8,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="60">
  <objectgroup draworder="index" id="2">
   <object id="1" x="4" y="16">
    <polyline points="0,0 0,-8 8,-8 8,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="61">
  <objectgroup draworder="index" id="2">
   <object id="1" x="16" y="8">
    <polyline points="0,0 -8,0 -8,8"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="62">
  <objectgroup draworder="index" id="2">
   <object id="1" x="16" y="16">
    <polyline points="0,0 0,-8 -16,-8"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="63">
  <objectgroup draworder="index" id="2">
   <object id="1" x="16" y="8">
    <polyline points="0,0 -16,0 -16,8"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="64">
  <objectgroup draworder="index" id="2">
   <object id="1" x="16" y="8">
    <polyline points="0,0 -16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="65">
  <objectgroup draworder="index" id="2">
   <object id="1" x="16" y="8">
    <polyline points="0,0 -16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="66">
  <objectgroup draworder="index" id="2">
   <object id="1" x="16" y="16">
    <polyline points="0,0 0,-8 -16,-8"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="78">
  <objectgroup draworder="index" id="2">
   <object id="1" x="4" y="16">
    <polyline points="0,0 0,-16"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="80">
  <objectgroup draworder="index" id="2">
   <object id="1" x="12" y="0">
    <polyline points="0,0 0,20"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="81">
  <objectgroup draworder="index" id="2">
   <object id="1" x="4" y="0">
    <polyline points="0,0 0,16"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="83">
  <objectgroup draworder="index" id="2">
   <object id="1" x="12" y="16">
    <polyline points="0,0 0,-16"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="84">
  <objectgroup draworder="index" id="2">
   <object id="1" x="4" y="16">
    <polyline points="0,0 0,-16"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="85">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="8">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="86">
  <objectgroup draworder="index" id="2">
   <object id="1" x="12" y="0">
    <polyline points="0,0 0,16"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="87">
  <objectgroup draworder="index" id="2">
   <object id="1" x="8" y="0">
    <polyline points="0,0 0,16"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="88">
  <objectgroup draworder="index" id="2">
   <object id="1" x="16" y="16">
    <polyline points="0,0 0,-16"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="89">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="8">
    <polyline points="0,-8 0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="90">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="8">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="91">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="8">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="92">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="8">
    <polyline points="0,0 16,0 16,-8"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="104">
  <objectgroup draworder="index" id="2">
   <object id="2" x="0" y="4">
    <polyline points="4,-4 4,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="105">
  <objectgroup draworder="index" id="2">
   <object id="2" x="0" y="4">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="106">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="4">
    <polyline points="0,0 12,0"/>
   </object>
   <object id="2" x="12" y="4">
    <polyline points="0,0 0,-4"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="107">
  <objectgroup draworder="index" id="2">
   <object id="1" x="4" y="4">
    <polyline points="12,0 0,0 0,-4"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="108">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="4">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="109">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="4">
    <polyline points="0,0 12,0 12,-4"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="110">
  <objectgroup draworder="index" id="2">
   <object id="1" x="4" y="4">
    <polyline points="0,-4 0,0 12,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="111">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="4">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="112">
  <objectgroup draworder="index" id="2">
   <object id="1" x="12" y="0">
    <polyline points="0,0 0,4 -12,4"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="113">
  <objectgroup draworder="index" id="2">
   <object id="1" x="8" y="4">
    <polyline points="0,-4 0,0 8,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="114">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="4">
    <polyline points="0,0 16,0 16,-4"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="136">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="16">
    <polyline points="0,0 0,-8 16,-8"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="137">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="8">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="138">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="8">
    <polyline points="0,0 16,0 16,8"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="156">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="4">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="157">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="4">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="158">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="4">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="159">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="4">
    <polyline points="0,0 16,0 16,-4 0,-4 0,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="160">
  <objectgroup draworder="index" id="2">
   <object id="1" x="4" y="8">
    <polyline points="0,0 -4,-4 0,-8 8,-8 12,-4 8,0 0,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="162">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="16">
    <polyline points="0,0 0,-16"/>
   </object>
   <object id="2" x="16" y="16">
    <polyline points="0,0 0,-16"/>
   </object>
   <object id="3" x="16" y="0">
    <polyline points="0,0 0,16"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="163">
  <objectgroup draworder="index" id="2">
   <object id="1" x="16" y="0">
    <polyline points="0,0 -16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="164">
  <objectgroup draworder="index" id="2">
   <object id="1" x="16" y="0">
    <polyline points="0,0 0,16"/>
   </object>
   <object id="2" x="0" y="16">
    <polyline points="0,0 0,-16"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="188">
  <objectgroup draworder="index" id="2">
   <object id="1" x="16" y="4">
    <polyline points="0,-4 0,0 -16,0 -16,-4"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="190">
  <objectgroup draworder="index" id="2">
   <object id="1" x="16" y="0">
    <polyline points="0,0 0,4 -16,4 -16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="191">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="4">
    <polyline points="0,-4 0,0 16,0 16,-4"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="192">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="4">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="235">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="237">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="238">
  <objectgroup draworder="index" id="2">
   <object id="1" x="4" y="0">
    <polyline points="0,0 12,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="239">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0">
    <polyline points="0,0 12,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="266">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="267">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="268">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="270">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="271">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="272">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="312">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="313">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="314">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="317">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="16">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="318">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="16">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="319">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="16">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="342">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="8">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="343">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="8">
    <polyline points="0,8 0,0 16,0"/>
   </object>
   <object id="2" x="0" y="8">
    <polyline points="0,0 0,-8"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="345">
  <objectgroup draworder="index" id="2">
   <object id="1" x="16" y="0">
    <polyline points="0,0 0,16"/>
   </object>
   <object id="2" x="16" y="8">
    <polyline points="0,0 -16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="349">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="16">
    <polyline points="0,0 0,-16"/>
   </object>
   <object id="2" x="16" y="16">
    <polyline points="0,0 0,-16"/>
   </object>
   <object id="3" x="0" y="4">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="364">
  <objectgroup draworder="index" id="2">
   <object id="1" x="12" y="16">
    <polyline points="0,0 4,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="365">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="16">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="367">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="372">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="373">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="374">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="375">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="4">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="390">
  <objectgroup draworder="index" id="2">
   <object id="1" x="12" y="0">
    <polyline points="0,0 0,16"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="391">
  <objectgroup draworder="index" id="2">
   <object id="1" x="16" y="0">
    <polyline points="0,0 0,16"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="392">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="8">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="416">
  <objectgroup draworder="index" id="2">
   <object id="1" x="12" y="0">
    <polyline points="0,0 0,16 4,16"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="417">
  <objectgroup draworder="index" id="2">
   <object id="1" x="16" y="0">
    <polyline points="0,0 0,16 -16,16"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="419">
  <objectgroup draworder="index" id="2">
   <object id="1" x="12" y="16">
    <polyline points="4,0 0,0 0,-8 4,-8"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="420">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="16">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="421">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="16">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="470">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="12">
    <polyline points="0,-12 0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="471">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="12">
    <polyline points="0,0 16,0 16,-12"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="548">
  <objectgroup draworder="index" id="2">
   <object id="1" x="4" y="12">
    <polyline points="0,0 12,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="549">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="12">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="551">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="12">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="552">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="12">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="553">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="12">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="554">
  <objectgroup draworder="index" id="2">
   <object id="1" x="16" y="12">
    <polyline points="0,0 -16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="555">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="12">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="556">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="12">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="624">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="8">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="625">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="8">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="626">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="8">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="627">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="8">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="628">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="8">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="629">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="8">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="630">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="8">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="631">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="8">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="632">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="8">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="633">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="8">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="634">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="8">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="635">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="8">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="702">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="703">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="704">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0">
    <polyline points="0,0 16,0"/>
   </object>
  </objectgroup>
 </tile>
</tileset>
