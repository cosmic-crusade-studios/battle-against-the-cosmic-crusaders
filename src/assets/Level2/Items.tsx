<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.10" tiledversion="1.10.0" name="Items" tilewidth="16" tileheight="16" tilecount="324" columns="18">
 <image source="../Items.png" width="300" height="300"/>
 <tile id="0">
  <animation>
   <frame tileid="0" duration="150"/>
   <frame tileid="18" duration="150"/>
   <frame tileid="36" duration="150"/>
   <frame tileid="54" duration="150"/>
   <frame tileid="72" duration="75"/>
   <frame tileid="90" duration="75"/>
   <frame tileid="108" duration="75"/>
   <frame tileid="126" duration="75"/>
   <frame tileid="144" duration="150"/>
   <frame tileid="162" duration="150"/>
   <frame tileid="180" duration="150"/>
   <frame tileid="198" duration="150"/>
  </animation>
 </tile>
 <tile id="1">
  <animation>
   <frame tileid="1" duration="150"/>
   <frame tileid="19" duration="150"/>
   <frame tileid="37" duration="150"/>
   <frame tileid="55" duration="150"/>
   <frame tileid="73" duration="150"/>
   <frame tileid="91" duration="150"/>
   <frame tileid="109" duration="150"/>
  </animation>
 </tile>
 <tile id="2">
  <animation>
   <frame tileid="2" duration="500"/>
   <frame tileid="20" duration="150"/>
   <frame tileid="38" duration="150"/>
   <frame tileid="56" duration="150"/>
   <frame tileid="74" duration="150"/>
   <frame tileid="92" duration="500"/>
   <frame tileid="110" duration="150"/>
   <frame tileid="128" duration="150"/>
   <frame tileid="146" duration="150"/>
   <frame tileid="164" duration="150"/>
  </animation>
 </tile>
 <tile id="3">
  <animation>
   <frame tileid="3" duration="500"/>
   <frame tileid="21" duration="150"/>
   <frame tileid="39" duration="150"/>
   <frame tileid="57" duration="150"/>
   <frame tileid="75" duration="150"/>
   <frame tileid="93" duration="500"/>
   <frame tileid="111" duration="150"/>
   <frame tileid="129" duration="150"/>
   <frame tileid="147" duration="150"/>
   <frame tileid="165" duration="150"/>
  </animation>
 </tile>
</tileset>
