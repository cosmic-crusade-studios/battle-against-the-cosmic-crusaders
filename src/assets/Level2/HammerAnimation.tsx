<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.10" tiledversion="1.10.0" name="HammerAnimation" tilewidth="16" tileheight="16" tilecount="168" columns="8">
 <image source="HammerAnimation.png" width="128" height="339"/>
 <tile id="0">
  <animation>
   <frame tileid="80" duration="100"/>
   <frame tileid="96" duration="900"/>
   <frame tileid="0" duration="350"/>
   <frame tileid="32" duration="200"/>
   <frame tileid="48" duration="450"/>
  </animation>
 </tile>
 <tile id="1">
  <animation>
   <frame tileid="81" duration="100"/>
   <frame tileid="97" duration="300"/>
   <frame tileid="113" duration="300"/>
   <frame tileid="129" duration="300"/>
   <frame tileid="1" duration="200"/>
   <frame tileid="17" duration="150"/>
   <frame tileid="33" duration="200"/>
   <frame tileid="49" duration="200"/>
   <frame tileid="65" duration="250"/>
  </animation>
 </tile>
 <tile id="8">
  <animation>
   <frame tileid="88" duration="100"/>
   <frame tileid="104" duration="300"/>
   <frame tileid="120" duration="600"/>
   <frame tileid="8" duration="1000"/>
  </animation>
 </tile>
 <tile id="9">
  <animation>
   <frame tileid="89" duration="100"/>
   <frame tileid="105" duration="300"/>
   <frame tileid="121" duration="300"/>
   <frame tileid="137" duration="300"/>
   <frame tileid="9" duration="200"/>
   <frame tileid="25" duration="150"/>
   <frame tileid="41" duration="200"/>
   <frame tileid="57" duration="200"/>
   <frame tileid="73" duration="250"/>
  </animation>
 </tile>
 <tile id="10">
  <animation>
   <frame tileid="90" duration="100"/>
   <frame tileid="106" duration="300"/>
   <frame tileid="122" duration="600"/>
   <frame tileid="10" duration="1000"/>
  </animation>
 </tile>
 <tile id="11">
  <animation>
   <frame tileid="91" duration="100"/>
   <frame tileid="107" duration="300"/>
   <frame tileid="123" duration="600"/>
   <frame tileid="11" duration="1000"/>
  </animation>
 </tile>
 <tile id="12">
  <animation>
   <frame tileid="92" duration="100"/>
   <frame tileid="108" duration="300"/>
   <frame tileid="124" duration="600"/>
   <frame tileid="12" duration="1000"/>
  </animation>
 </tile>
</tileset>
