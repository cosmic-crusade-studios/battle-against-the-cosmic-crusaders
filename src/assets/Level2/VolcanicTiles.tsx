<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.10" tiledversion="1.10.1" name="VolcanicTiles" tilewidth="16" tileheight="16" tilecount="180" columns="10">
 <image source="VolcanicTiles.png" width="160" height="288"/>
 <tile id="62">
  <animation>
   <frame tileid="62" duration="450"/>
   <frame tileid="63" duration="450"/>
   <frame tileid="64" duration="450"/>
   <frame tileid="65" duration="450"/>
  </animation>
 </tile>
 <tile id="72">
  <animation>
   <frame tileid="72" duration="450"/>
   <frame tileid="73" duration="450"/>
   <frame tileid="74" duration="450"/>
   <frame tileid="75" duration="450"/>
  </animation>
 </tile>
 <tile id="82">
  <animation>
   <frame tileid="82" duration="450"/>
   <frame tileid="83" duration="450"/>
   <frame tileid="84" duration="450"/>
   <frame tileid="85" duration="450"/>
  </animation>
 </tile>
 <tile id="92">
  <animation>
   <frame tileid="92" duration="450"/>
   <frame tileid="93" duration="450"/>
   <frame tileid="94" duration="450"/>
   <frame tileid="95" duration="450"/>
  </animation>
 </tile>
 <tile id="142">
  <animation>
   <frame tileid="142" duration="450"/>
   <frame tileid="143" duration="450"/>
   <frame tileid="144" duration="450"/>
   <frame tileid="145" duration="450"/>
  </animation>
 </tile>
 <tile id="152">
  <animation>
   <frame tileid="152" duration="450"/>
   <frame tileid="153" duration="450"/>
   <frame tileid="154" duration="450"/>
   <frame tileid="155" duration="450"/>
  </animation>
 </tile>
 <tile id="162">
  <animation>
   <frame tileid="162" duration="450"/>
   <frame tileid="163" duration="450"/>
   <frame tileid="164" duration="450"/>
   <frame tileid="165" duration="450"/>
  </animation>
 </tile>
</tileset>
