<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.10" tiledversion="1.10.0" name="GrassTiles" tilewidth="16" tileheight="16" tilecount="20" columns="5">
 <image source="GrassTiles.png" width="80" height="64"/>
 <tile id="2">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="16">
    <polygon points="0,0 16,0 16,-16 0,-16"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="3">
  <objectgroup draworder="index" id="2">
   <object id="1" x="16" y="16">
    <polygon points="0,0 -12,0 -12,-12 0,-12"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="4">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="16">
    <polygon points="0,0 16,0 16,-12 0,-12"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="5">
  <objectgroup draworder="index" id="2">
   <object id="3" x="-0.125" y="3.875">
    <polygon points="0,0 12.125,0.125 12.125,12.125 0.125,12.125"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="6">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0">
    <polygon points="0,0 16,0 16,16 0,16"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="7">
  <objectgroup draworder="index" id="2">
   <object id="1" x="16" y="16">
    <polygon points="0,0 0,-16 -12,-16 -12,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="8">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0">
    <polygon points="0,0 0,16 12,16 12,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="9">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0">
    <polygon points="0,0 0,16 16,16 16,4 12,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="10">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="16">
    <polygon points="0,0 0,-12 4,-16 16,-16 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="11">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="13.875">
    <polygon points="0,0 15.875,0.125 16,-13.875 0,-13.875"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="12">
  <objectgroup draworder="index" id="2">
   <object id="1" x="16" y="0">
    <polygon points="0,0 -12,0 -12,12 0,12"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="13">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0">
    <polygon points="0,0 12,0 12,12 0,12"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="14">
  <objectgroup draworder="index" id="2">
   <object id="2" x="1.75" y="2.125">
    <polygon points="0,0 0.25,12.625 12.125,12.75 12.125,-0.125"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="15">
  <objectgroup draworder="index" id="2">
   <object id="1" x="16.125" y="14.75">
    <polygon points="0,0 -14.75,0 -14.75,-12.75 -0.125,-12.5"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="16">
  <objectgroup draworder="index" id="2">
   <object id="2" x="0" y="13.875">
    <polygon points="0,0 12.75,0.125 12.625,-11.875 0,-11.875"/>
   </object>
  </objectgroup>
 </tile>
</tileset>
