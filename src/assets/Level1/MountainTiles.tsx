<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.10" tiledversion="1.10.0" name="MountainTiles" tilewidth="16" tileheight="16" tilecount="25" columns="25">
 <image source="MountainTiles.png" width="400" height="16"/>
 <tile id="7">
  <objectgroup draworder="index" id="5">
   <object id="8" x="0.363636" y="3.63636" width="16" height="12.7273"/>
  </objectgroup>
 </tile>
 <tile id="14">
  <objectgroup draworder="index" id="2">
   <object id="1" x="15.8182" y="1.63636">
    <polygon points="-0.181818,-1.09091 0.181818,14.3636 -15.4545,14.1818"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="15">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0.363636">
    <polygon points="0,0 15.6364,15.8182 0,15.6384"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="16">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0">
    <polygon points="-0.181818,0.363636 15.8182,0 16,16.5455"/>
   </object>
  </objectgroup>
 </tile>
</tileset>
