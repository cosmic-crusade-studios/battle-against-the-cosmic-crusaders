package inf112.skeleton.app;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.headless.HeadlessApplication;
import com.badlogic.gdx.backends.headless.HeadlessApplicationConfiguration;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import inf112.skeleton.app.Levels.PlatformLevel;
import inf112.skeleton.app.Player.PlatformPlayer;
import inf112.skeleton.app.Player.SpaceshipPlayer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.RunWith;
import org.junit.runner.notification.Failure;
import org.junit.runners.Suite;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        PlayerTests.class,
        LevelTests.class,
        StartupTests.class,
        NPCTests.class
})
public class AppTest {

    JUnitCore junit = new JUnitCore();


    public static final Vector2 spawnPos = new Vector2(0,0);
    public static final TestGame testGame = new TestGame();
    private World world;

    /**
     * Static method run before everything else
     */

    @BeforeAll
    static void setUpBeforeAll() {

        Lwjgl3ApplicationConfiguration cfg = new Lwjgl3ApplicationConfiguration();
        cfg.setInitialVisible(false);
        new Lwjgl3Application(testGame, cfg);

        HeadlessApplicationConfiguration conf = new HeadlessApplicationConfiguration();
        new HeadlessApplication(testGame, conf);
    }

    /**
     * Test that Gdx.files are initiated
     */
    @Test
    public void testFiles() {
        assertNotNull(Gdx.files);
    }

    /**
     * Test that graphics gl is working
     * */
    @Test
    public void testGL20(){
        assertNotNull(Gdx.gl);
    }
    /**
    @Test
    public void RunAllTestFiles() {
        Result result = JUnitCore.runClasses(TestSuite.class);
        for (Failure failure : result.getFailures()) {
            System.out.println(failure.toString());
        }

        resultReport(result);
        System.out.println(result.wasSuccessful());

    }

    public static void resultReport(Result result) {
        System.out.println("Finished. Result: Failures: " +
                result.getFailureCount() + ". Ignored: " +
                result.getIgnoreCount() + ". Tests run: " +
                result.getRunCount() + ". Time: " +
                result.getRunTime() + "ms.");
    }
    */

}