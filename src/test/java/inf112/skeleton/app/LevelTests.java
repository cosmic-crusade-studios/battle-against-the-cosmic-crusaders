package inf112.skeleton.app;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.backends.headless.HeadlessFiles;
import com.badlogic.gdx.backends.headless.mock.graphics.MockGraphics;
import com.badlogic.gdx.backends.lwjgl3.audio.mock.MockAudio;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.utils.GdxNativesLoader;
import inf112.skeleton.app.Levels.PlatformLevel;
import inf112.skeleton.app.Levels.Spaceship;
import inf112.skeleton.app.Screens.IntroScreen;
import org.junit.Before;
import org.junit.Test;
import com.badlogic.gdx.ApplicationAdapter;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class LevelTests extends ApplicationAdapter {

    IntroScreen introScreen;
    private GameSetup game;
    //World world;
    OrthogonalTiledMapRenderer levelRenderer;
    private PlatformLevel level;

    /**
     * Static method run before everything else
     */

    @Override
    public void create() {
        // Initialization code here
    }

    @Override
    public void render() {
        // Rendering code here
    }

    @Override
    public void dispose() {
        // Cleanup code here
    }

    @Before
    public void setUp() {

        Gdx.files = new HeadlessFiles();
        Gdx.audio = new MockAudio();
        Gdx.graphics = new MockGraphics();
        GL20 mockGl = mock(GL20.class);
        Gdx.gl = mockGl;
        Gdx.gl20 = mockGl;
        GdxNativesLoader.load();
        Gdx.graphics.setContinuousRendering(false);
        Gdx.input = mock(Input.class);

        game = new GameSetup();
        SpriteBatch mockBatch = mock(SpriteBatch.class);
        Color expectedColor = Color.WHITE;
        when(mockBatch.getColor()).thenReturn(expectedColor);
        game.batch = mockBatch;
        //game.batch = mock(SpriteBatch.class);
        introScreen = new IntroScreen(game);
        game.setScreen(introScreen);

    }


    @Test
    public void makeMapWithFactory() {
        World world = new World(new Vector2(0, 0), true);
        //String map = "src/assets/Testmaps/test_map_top_down.tmx";
        String map = "src/assets/Level2/Level2Part1.tmx";
        this.levelRenderer = MapFactory.createLevel(map, world, game);
        //System.out.println(levelRenderer);
    }

    @Test
    public void testPlatformLevel() {
        String map = "src/assets/Level2/Level2Part1.tmx";
        String[] parallaxesPaths = new String[3];
        parallaxesPaths[0] = "src/assets/Level2/Sky.png";
        parallaxesPaths[1] = "src/assets/Level2/Back.png";
        parallaxesPaths[2] = "src/assets/Level2/Middle.png";
        float[][] xyFactors = {
                {0.05f, 0f},
                {0.1f, 0f},
                {0.35f, -0.2f}
        };
        int[] spawnpoints = {-150, -150, 0};
        level = new PlatformLevel(game, map, parallaxesPaths, "src/assets/audio/soundtrack/Volcanic1.wav", xyFactors, spawnpoints, false, -12f);
        game.setScreen(level);
        assertTrue(game.getScreen() instanceof PlatformLevel);

        level.getColliderListener().setItemAction(true);
        try {
            level.render(1 / 60f);
        }
        catch (Exception e){

        }
        level.dispose();
    }

    @Test
    public void testSpaceshipLevel(){
        String map = "src/assets/Spaceship/rooms/CargoBay.tmx";
        Spaceship spaceship = new Spaceship(game, map);
        game.setScreen(spaceship);
        assertTrue(game.getScreen() instanceof Spaceship);
        spaceship.nextLevel = "src/assets/Spaceship/rooms/CargoBay.tmx";
        spaceship.render(1/60f);
    }

    @Test
    public void testChangeRoom(){
        String map = "src/assets/Level2/Level2Part1.tmx";
        String[] parallaxesPaths = new String[3];
        parallaxesPaths[0] = "src/assets/Level2/Sky.png";
        parallaxesPaths[1] = "src/assets/Level2/Back.png";
        parallaxesPaths[2] = "src/assets/Level2/Middle.png";
        float[][] xyFactors = {
                {0.05f, 0f},
                {0.1f, 0f},
                {0.35f, -0.2f}
        };
        int[] spawnpoints = {-150, -150, 0};
        PlatformLevel platformLevel = new PlatformLevel(game, map, parallaxesPaths, "src/assets/audio/soundtrack/Volcanic1.wav", xyFactors, spawnpoints, false, -12f);
        game.setScreen(level);

        platformLevel.getColliderListener().nextLevel = "src/assets/Level2/Level2Part1.tmx";
        platformLevel.getColliderListener().setRoomChange(true);
        try {
            platformLevel.render(1 / 60f);
        }
        catch (Exception ignored){
        }

        platformLevel.getColliderListener().nextLevel = "src/assets/Level2/Level2Part2.tmx";
        platformLevel.getColliderListener().setRoomChange(true);
        try {
            platformLevel.render(1 / 60f);
        }
        catch (Exception ignored){
        }

        platformLevel.getColliderListener().nextLevel = "RoomSelection";
        platformLevel.getColliderListener().setRoomChange(true);
        try {
            platformLevel.render(1 / 60f);
        }
        catch (Exception ignored){
        }

        platformLevel.getColliderListener().nextLevel = "Platform";
        platformLevel.getColliderListener().setRoomChange(true);
        try {
            platformLevel.render(1 / 60f);
        }
        catch (Exception ignored){
        }

        platformLevel.getColliderListener().nextLevel = "GameOver";
        platformLevel.getColliderListener().setRoomChange(true);
        try {
            platformLevel.render(1 / 60f);
        }
        catch (Exception ignored){
        }
    }
}
