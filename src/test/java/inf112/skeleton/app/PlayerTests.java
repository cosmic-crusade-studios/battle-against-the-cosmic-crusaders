package inf112.skeleton.app;

import com.badlogic.gdx.backends.headless.HeadlessApplication;
import com.badlogic.gdx.backends.headless.HeadlessApplicationConfiguration;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import inf112.skeleton.app.Player.PlatformPlayer;
import inf112.skeleton.app.Player.SpaceshipPlayer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PlayerTests {


    public static final TestGame testGame = new TestGame();
    private World world;

    /**
     * Static method run before everything else
     */

    @BeforeAll
    static void setUpBeforeAll() {

        Lwjgl3ApplicationConfiguration cfg = new Lwjgl3ApplicationConfiguration();
        cfg.setInitialVisible(false);
        new Lwjgl3Application(testGame, cfg);

        HeadlessApplicationConfiguration conf = new HeadlessApplicationConfiguration();
        new HeadlessApplication(testGame, conf);
    }

    /**
     * Setup method called before each of the test methods
     */
    //@BeforeEach
    //void setUpBeforeEach() {

    //}

    @AfterEach
    void cleanUpAfterEach() {
        if (this.world != null) {
            world.dispose();
        }
    }

    /**
     * Test gravity
     */
    @Test
    void testGravity() {
        world = new World(new Vector2(0, -9.81f), false);

        PlatformPlayer player = new PlatformPlayer(0, 10, world);
        for (int i = 0; i < 60; i++) {
            world.step(1 / 60f, 6, 2);
            player.update();
        }


        assertEquals(0f, player.getX(), 0.1f);
        assertEquals(5.1f, player.getY() / Constants.PPM, 0.1f);


    }

    @Test
    void initiateSpaceshipPlayer() {
        world = new World(new Vector2(0, 0), true);
        SpaceshipPlayer player = new SpaceshipPlayer(10, 10, world);
        world.step(1 / 60f, 6, 2);
        player.update();

        assertEquals(10f, player.getX() / Constants.PPM);
        assertEquals(10f, player.getY() / Constants.PPM);
    }

    @Test
    void initiatePlatformPlayer() {
        world = new World(new Vector2(0, -9.81f), true);
        SpaceshipPlayer player = new SpaceshipPlayer(0, 0, world);
        world.step(1 / 60f, 6, 2);
        player.update();

        assertEquals(0f, player.getX() / Constants.PPM);
        assertEquals(0f, player.getY() / Constants.PPM, 0.1f);
    }

    /**
     * checking of we can walk out of bound either in x or y direction at 0
     */

    @Test
    void checkOutOfBoundsOnZero() {

        // Expected result is the first argument, value to be tested is the second.
        // The message is optional.
    }

    /**
     * checking of we can walk out of bound either in x or y direction at max values
     */
    @Test
    void checkOutOfBoundsOnMax() {
        // For floats and doubles it's best to use assertEquals with a delta, since
        // floating-point numbers are imprecise
    }

    /**
     * Checking each of the movement options for the spaceship player object
     */
    @Test
    void movementSpaceshipPlayer() {
        world = new World(new Vector2(0,0),true);
        SpaceshipPlayer player = new SpaceshipPlayer(10,10,world);
        world.step(1 / 60f, 6, 2);
        player.update();
         //Checking movement left

        for (int i = 0; i < 160; i++) {
            player.moveLeft();
            player.updateVelocity();
            world.step(1 / 60f, 6, 2);

        }
        player.update();
        assertEquals(8f, player.getX() / Constants.PPM, 0.1f);
        //Checking movement down
        for (int i = 0; i < 160; i++) {
            player.moveDown();
            player.updateVelocity();
            world.step(1 / 60f, 6, 2);
        }
        player.update();
        assertEquals(8f,player.getY()/Constants.PPM,0.1f);

        //Checking setPosition
        player.setPosition(10,10);
        player.update();
        assertEquals(10, player.getX()/Constants.PPM);
        assertEquals(10, player.getY()/Constants.PPM);

        player.standStill();
        player.updateVelocity();

        //Checking movement right
        for (int i = 0; i < 160; i++) {
            player.moveRight();
            player.updateVelocity();
            world.step(1 / 60f,6,2);
        }
        player.update();
        assertEquals(12f,player.getX()/Constants.PPM,0.1f);
        //Checking movement up
        for (int i = 0; i < 160; i++) {
            player.moveUp();
            player.updateVelocity();
            world.step(1 / 60f,6,2);
        }
        player.update();
        assertEquals(12f,player.getY()/Constants.PPM,0.1f);
    }

    /**
     * Test sprint
     */
    @Test
    void sprintTestSpace() {
        world = new World(new Vector2(0, 0), true);
        SpaceshipPlayer player = new SpaceshipPlayer(10, 10, world);
        world.step(1 / 60f, 6, 2);
        player.update();
        player.moveRight();
        player.moveUp();
        world.step(1 / 60f, 6, 2);
        player.update();
        float normalSpeedX = player.getxVelocity();
        float normalSpeedY = player.getyVelocity();
        player.moveRight();
        player.moveUp();
        player.sprint();

        world.step(1 / 60f, 6, 2);
        player.update();
        float sprintSpeedX = player.getxVelocity();
        float sprintSpeedY = player.getyVelocity();
        assertEquals(normalSpeedX * 2f, sprintSpeedX);
        assertEquals(normalSpeedY * 2f, sprintSpeedY);
    }

    /**
     * Checking if the games jump functionality is working
     */
    @Test
    void checkJump() {
        world = new World(new Vector2(0, -9.81f), false);
        PlatformPlayer player = new PlatformPlayer(0, 0, world);
        player.setJumpPower(5);
        //world.step(1 / 60f, 6, 2);
        //player.update();

        player.jump();
        for (int i = 0; i < 15; i++) {
            world.step(1 / 60f, 6, 2);
            player.update();
        }

        assertEquals(0, player.getX() / Constants.PPM);
        assertEquals(32 / Constants.PPM, player.getY() / Constants.PPM, 0.1f);

    }

    @Test
    void checkPlayerTexture() {
        World platformWorld = new World(new Vector2(0, -9.81f), false);
        World spaceshipWorld = new World(new Vector2(0, 0), false);
        PlatformPlayer player = new PlatformPlayer(0, 0, platformWorld);
        SpaceshipPlayer player2 = new SpaceshipPlayer(0, 0, spaceshipWorld);
        platformWorld.step(1 / 60f, 6, 2);
        spaceshipWorld.step(1 / 60f, 6, 2);
        player.update();
        player2.update();
        Texture sheet = new Texture("src/assets/BossSpriteSheet.png");
        TextureRegion[][] playerTextureSheet = TextureRegion.split(
                sheet,
                sheet.getWidth() / 4,
                sheet.getHeight() / 6
        );

        player.setPlayerTextureSheet(playerTextureSheet);
        assertEquals(playerTextureSheet[4][0], player.getPlayerTexture());

        player2.setPlayerTextureSheet(playerTextureSheet);
        assertEquals(playerTextureSheet[4][0], player2.getPlayerTexture());
    }
}
