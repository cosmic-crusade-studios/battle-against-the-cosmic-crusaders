package inf112.skeleton.app;

import com.badlogic.gdx.backends.headless.HeadlessApplication;
import com.badlogic.gdx.backends.headless.HeadlessApplicationConfiguration;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import inf112.skeleton.app.NPCs.Ant;
import inf112.skeleton.app.NPCs.Frog;
import inf112.skeleton.app.NPCs.Goat;
import inf112.skeleton.app.Player.PlatformPlayer;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;

import static org.junit.jupiter.api.Assertions.*;

public class NPCTests {

    public static final TestGame testGame = new TestGame();
    private World world;

    /**
     * Static method run before everything else
     */
    @BeforeAll
    static void setUpBeforeAll() {

        Lwjgl3ApplicationConfiguration cfg = new Lwjgl3ApplicationConfiguration();
        cfg.setInitialVisible(false);
        new Lwjgl3Application(testGame, cfg);

        HeadlessApplicationConfiguration conf = new HeadlessApplicationConfiguration();
        new HeadlessApplication(testGame, conf);
    }



    @Test
    public void NPCMovementTest() {
        world = new World(new Vector2(0, 0), true);
        Frog frog = new Frog(5, 5, world);
        Ant ant = new Ant(10, 10, world);
        Goat goat = new Goat(15, 15, world);
        world.step(1 / 60f, 6, 2);
        ant.update();
        frog.update();
        goat.update();

        for (int i = 0; i < 300; i++) {
            world.step(1 / 60f, 6, 2);
            ant.update();
            frog.update();
            goat.update();
        }
        assertNotEquals(5, frog.getX() / Constants.PPM);
        assertNotEquals(10, ant.getX() / Constants.PPM);
        assertNotEquals(15, goat.getX() / Constants.PPM);
    }


    @Test
    public void checkAntTexture() {
        world = new World(new Vector2(0, -9.81f), false);
        Ant ant = new Ant(0, 0, world);
        world.step(1 / 60f, 6, 2);
        ant.update();
        Texture sheet = new Texture("src/assets/graphics/Mobs/Ant.png");

        TextureRegion[][] antTextureSheet = TextureRegion.split(
                sheet,
                sheet.getWidth() / 4,
                sheet.getHeight() / 6
        );
        for (int i = 0; i < 4; i++) {

            if (ant.getTexture() == antTextureSheet[0][0] || ant.getTexture() == antTextureSheet[1][0] || ant.getTexture() == antTextureSheet[2][0] ||ant.getTexture() == antTextureSheet[3][0] || ant.getTexture() == antTextureSheet[4][0]){
                assertEquals(antTextureSheet[4][0], ant.getTexture());
            }
        }

    }

    @Test
    public void NPCDiesWhenJumpedOn() throws InterruptedException {
        world = new World(new Vector2(0, 0), false);
        ColliderContactListener contactListener = new ColliderContactListener("");
        world.setContactListener(contactListener);
        PlatformPlayer player = new PlatformPlayer(0, 10, world);
        Frog frog = new Frog(0, 0, world);
        player.update();
        frog.update();
        assertFalse(frog.killed);
        assertFalse(frog.despawned);
        player.setPosition(0, 16 / Constants.PPM);
        world.step(1, 6, 2);
        frog.update();
        assertTrue(frog.killed);
        assertFalse(frog.despawned);
        Thread.sleep(1001);
        world.step(1, 6, 2);
        frog.update();
        assertTrue(frog.killed);
        assertTrue(frog.despawned);
    }

}
