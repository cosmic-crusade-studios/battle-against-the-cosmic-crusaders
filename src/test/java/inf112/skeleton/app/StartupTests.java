package inf112.skeleton.app;

import com.badlogic.gdx.*;
import com.badlogic.gdx.backends.headless.HeadlessFiles;
import com.badlogic.gdx.backends.headless.mock.graphics.MockGraphics;
import com.badlogic.gdx.backends.lwjgl3.audio.mock.MockAudio;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.utils.GdxNativesLoader;
import inf112.skeleton.app.Screens.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class StartupTests extends ApplicationAdapter {
    IntroScreen introScreen;
    private GameSetup game;

    @Override
    public void create() {
        // Initialization code here
    }

    @Override
    public void render() {
        // Rendering code here
    }

    @Override
    public void dispose() {
        // Cleanup code here
    }

    @Before
    public void setUp() {
        Gdx.app = mock(Application.class);
        Gdx.files = new HeadlessFiles();
        Gdx.audio = new MockAudio();
        Gdx.graphics = new MockGraphics();
        GL20 mockGl = mock(GL20.class);
        Gdx.gl = mockGl;
        Gdx.gl20 = mockGl;
        GdxNativesLoader.load();
        Gdx.graphics.setContinuousRendering(false);
        Input mockInput = mock(Input.class);
        Gdx.input = mockInput;

        game = new GameSetup();
        SpriteBatch mockBatch = mock(SpriteBatch.class);
        //Color expectedColor = Color.WHITE;
        //when(mockBatch.getColor()).thenReturn(expectedColor);

        //Matrix4 mockMatrix = mock(Matrix4.class);
        //when(mockBatch.getProjectionMatrix()).thenReturn(mockMatrix);
        game.batch = mockBatch;

    }

    @After
    public void tearDown() {
        game.dispose();
    }

    @Test
    public void testIntroScreen() {
        introScreen = new IntroScreen(game);
        game.setScreen(introScreen);
        assertTrue(game.getScreen() instanceof IntroScreen);
        for (int i = 0; i < 1000; i++)introScreen.render(1/60f);
        introScreen.dispose();
    }

    @Test
    public void testMainMenuScreen() {
        MainMenuScreen mainMenuScreen = new MainMenuScreen(game);
        game.setScreen(mainMenuScreen);
        assertTrue(game.getScreen() instanceof MainMenuScreen);
        try {
            mainMenuScreen.render(1/60f);
        } catch (NullPointerException e) {}
        mainMenuScreen.dispose();
    }

    @Test
    public void testCreditsScreen() {
        CreditsScreen creditsScreen = new CreditsScreen(game);
        game.setScreen(creditsScreen);
        assertTrue(game.getScreen() instanceof CreditsScreen);
        try {
            creditsScreen.render(1 / 60f);
        } catch (NullPointerException e) {}
        creditsScreen.dispose();
    }

    @Test
    public void testGameOverScreen() {
        GameOverScreen gameOverScreen = new GameOverScreen(game);
        game.setScreen(gameOverScreen);
        assertTrue(game.getScreen() instanceof GameOverScreen);
        try {
            gameOverScreen.render(1/60f);
        } catch (NullPointerException e) {}
        gameOverScreen.dispose();
    }

    @Test
    public void testHelpScreen() {
        HelpScreen helpScreen = new HelpScreen(game);
        game.setScreen(helpScreen);
        assertTrue(game.getScreen() instanceof HelpScreen);
        try {
            helpScreen.render(1/60f);
        } catch (NullPointerException e) {}
        helpScreen.dispose();
    }

    @Test
    public void testLevelSelectionScreen() {
        LevelSelectionScreen levelSelectionScreen = new LevelSelectionScreen(game);
        game.setScreen(levelSelectionScreen);
        assertTrue(game.getScreen() instanceof LevelSelectionScreen);
        try {
            levelSelectionScreen.render(1/60f);
        } catch (NullPointerException e) {}
        levelSelectionScreen.dispose();
    }

    @Test
    public void testRoomSelectionScreen() {
        RoomSelectionScreen roomSelectionScreen = new RoomSelectionScreen(game);
        game.setScreen(roomSelectionScreen);
        assertTrue(game.getScreen() instanceof RoomSelectionScreen);
        try {
            roomSelectionScreen.render(1 / 60f);
        } catch (NullPointerException e) {}
        roomSelectionScreen.dispose();
    }
}
