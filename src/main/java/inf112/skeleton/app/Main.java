package inf112.skeleton.app;

import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;

public class Main {
    /**
     * Initializes the game by configuring the title and setting window dimensions, then starts the game setup.
     */
    public static void main(String[] args) {
        Lwjgl3ApplicationConfiguration cfg = new Lwjgl3ApplicationConfiguration();
        cfg.setTitle("Battle Against the Cosmic Crusaders");
        cfg.setIdleFPS(Constants.FPS);
        cfg.setMaximized(true);

        new Lwjgl3Application(new GameSetup(),cfg);
    }
}