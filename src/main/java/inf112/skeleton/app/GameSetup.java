package inf112.skeleton.app;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import inf112.skeleton.app.Screens.IntroScreen;


public class GameSetup extends Game {

    public SpriteBatch batch;
    public BitmapFont font;

    /**
     * Defines a SpriteBatch, BitmapFont and Cursor for the game, and starts the intro.
     */
    @Override
    public void create() {
        batch = new SpriteBatch();
        font = new BitmapFont();
        this.setScreen(new IntroScreen(this));
        Gdx.graphics.setCursor(
                Gdx.graphics.newCursor(new Pixmap(Gdx.files.internal("src/assets/Mouse.png")), 0, 0)
        );
    }
}
