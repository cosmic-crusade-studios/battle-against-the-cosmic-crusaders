package inf112.skeleton.app;

import com.badlogic.gdx.physics.box2d.*;

public class HitboxFactory {
    /**
     * Creates a hitbox
     * @param x x-coordinate of the hitbox
     * @param y y-coordinate of the hitbox
     * @param world what world the hitbox should be in
     * @param userData the userData property of the hitbox, should be something that makes it possible to identify
     *                 which object the hitbox belongs to
     * @param width width of the hitbox
     * @param height height of the hitbox
     * @return a hitbox
     */
    public static Body createHitbox(float x, float y, World world, Object userData, int width, int height) {
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.position.set(x, y);
        Body body = world.createBody(bodyDef);

        PolygonShape shape = new PolygonShape();
        shape.setAsBox(
                width * Constants.TILEWIDTH / (2 * Constants.PPM),
                height * Constants.TILEHEIGHT / (2 * Constants.PPM)
        );

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;
        fixtureDef.friction = 0;
        Fixture fixture = body.createFixture(fixtureDef);
        fixture.setUserData(userData);
        shape.dispose();

        return body;
    }
}
