package inf112.skeleton.app;


import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import inf112.skeleton.app.NPCs.AbstractHostileNPC;
import inf112.skeleton.app.NPCs.AbstractNPC;

public class ColliderContactListener implements ContactListener {
    public static String nextLevel;
    private int spawnIndex;
    private final String map;
    private boolean roomChange;
    private boolean itemAction;

    /**
     * Creates a handler for contact between fixtures.
     * @param map current map, used to calculate what the next map should be,
     *            based on what the player comes in contact with
     */
    public ColliderContactListener(String map) {
        this.map = map;
        this.roomChange = false;
        this.spawnIndex = 0;
    }

    /**
     * Handles contact between the Player and doors, NPCs and other deadly objects.
     * Handles contact between NPCs and invisible walls, so they stay confined.
     * @param contact object containing fixtures that made the contact
     */
    @Override
    public void beginContact(Contact contact) {
        Fixture fixtureA = contact.getFixtureA();
        Fixture fixtureB = contact.getFixtureB();

        Object userdataA = fixtureA.getUserData();
        Object userdataB = fixtureB.getUserData();

        if (
                (userdataA.equals("RoomExit") && userdataB.equals("Player")) ||
                (userdataA.equals("Player") && userdataB.equals("RoomExit"))
        ) {
            if (
                    map.equals("src/assets/Spaceship/rooms/Shuttle.tmx") ||
                    map.equals("src/assets/Level1/level1.tmx") ||
                    map.equals("src/assets/Level2/Level2Part1.tmx") ||
                    map.equals("src/assets/Level2/Level2Part2.tmx")
            ) {
                nextLevel = "src/assets/Spaceship/rooms/CargoBay.tmx";
                spawnIndex = 1;
            } else if (
                    !map.equals("src/assets/Spaceship/rooms/TurboLiftUp.tmx") &&
                    !map.equals("src/assets/Spaceship/rooms/TurboLiftDown.tmx") &&
                    !map.equals("src/assets/Spaceship/rooms/TurboLiftLeft.tmx") &&
                    !map.equals("src/assets/Spaceship/rooms/TurboLiftRight.tmx")
            ) {
                nextLevel = "RoomSelection";
            }

            roomChange = true;
        } else if (
                (userdataA.equals("Shuttle") && userdataB.equals("Player")) ||
                (userdataA.equals("Player") && userdataB.equals("Shuttle"))
        ) {
            nextLevel = "src/assets/Spaceship/rooms/Shuttle.tmx";
            roomChange = true;
        } else if (
                (userdataA.equals("Transport") && userdataB.equals("Player")) ||
                (userdataA.equals("Player") && userdataB.equals("Transport"))
        ) {
            nextLevel = "Platform";
            roomChange = true;
        } else if (
                (userdataA.equals("GameOver") && userdataB.equals("Player")) ||
                (userdataA.equals("Player") && userdataB.equals("GameOver"))
        ) {
            nextLevel = "GameOver";
            roomChange = true;
        } else if (
                (userdataA.equals("EnterCave") && userdataB.equals("Player")) ||
                (userdataA.equals("Player") && userdataB.equals("EnterCave"))
        ) {
            nextLevel = "src/assets/Level2/Level2Part2.tmx";
            roomChange = true;
        } else if (
                (userdataA.equals("EnterCave2") && userdataB.equals("Player")) ||
                (userdataA.equals("Player") && userdataB.equals("EnterCave2"))
        ) {
            nextLevel = "src/assets/Level2/Level2Part2.tmx";
            spawnIndex = 1;
            roomChange = true;
        } else if (
                (userdataA.equals("LeaveCave") && userdataB.equals("Player")) ||
                (userdataA.equals("Player") && userdataB.equals("LeaveCave"))
        ) {
            nextLevel = "src/assets/Level2/Level2Part1.tmx";
            spawnIndex = 1;
            roomChange = true;
        } else if (
                (userdataA.equals("LeaveCave2") && userdataB.equals("Player")) ||
                (userdataA.equals("Player") && userdataB.equals("LeaveCave2"))
        ) {
            nextLevel = "src/assets/Level2/Level2Part1.tmx";
            spawnIndex = 2;
            roomChange = true;
        }
        if (
                (userdataA.equals("Item") && userdataB.equals("Player")) ||
                (userdataA.equals("Player") && userdataB.equals("Item"))
        ) {
            itemAction = true;
        }
        if (userdataA instanceof AbstractNPC && userdataB.equals("NPCollision")) {
            ((AbstractNPC) userdataA).collision = true;
            ((AbstractNPC) userdataA).turnAround();
        } else if (userdataB instanceof AbstractNPC && userdataA.equals("NPCollision")) {
            ((AbstractNPC) userdataB).collision = true;
            ((AbstractNPC) userdataB).turnAround();
        }
        if (userdataA instanceof AbstractNPC && userdataB.equals("Player")) {
            handleNPCUserContact(contact, userdataA);
        } else if (userdataB instanceof AbstractNPC && userdataA.equals("Player")) {
            handleNPCUserContact(contact, userdataB);
        }
    }

    private void handleNPCUserContact(Contact contact, Object userdata) {
        WorldManifold manifold = contact.getWorldManifold();
        Vector2 normal = manifold.getNormal();
        if (normal.y < 0) {
            ((AbstractNPC) userdata).killed = true;
        } else if (userdata instanceof AbstractHostileNPC && !((AbstractHostileNPC) userdata).killed) {
            nextLevel = "GameOver";
            roomChange = true;
        }
    }

    /**
     * @return boolean value that tells if the room should be changed
     */
    public boolean isRoomChange() {
        return roomChange;
    }

    /**
     * @return boolean value that tells if the player has picked up an item
     */
    public boolean isItemAction() {
        return itemAction;
    }

    /**
     * Setter for the value that tells if the player has picked up an item.
     * @param itemAction boolean value that tells if the player has picked up an item
     */
    public void setItemAction(boolean itemAction) {
        this.itemAction = itemAction;
    }

    /**
     * Setter for value that tells if the room should be changed.
     * @param roomChange boolean value that tells if the room should be changed
     */
    public void setRoomChange(boolean roomChange) {
        this.roomChange = roomChange;
    }

    /**
     * @return the path of the next level
     */
    public String getNextLevel() {
        return nextLevel;
    }

    /**
     * @return index of that spawn that should be used, in case there are several options
     */
    public int getSpawnIndex() {
        return spawnIndex;
    }

    /**
     * Updates the state of NPCs that are no longer touching invisible walls meant to confine them.
     * @param contact object containing fixtures that made the contact
     */
    @Override
    public void endContact(Contact contact) {
        Fixture fixtureA = contact.getFixtureA();
        Fixture fixtureB = contact.getFixtureB();

        Object userdataA = fixtureA.getUserData();
        Object userdataB = fixtureB.getUserData();

        if (userdataA instanceof AbstractNPC && userdataB.equals("NPCollision")) {
            ((AbstractNPC) userdataA).collision = false;
        } else if (userdataB instanceof AbstractNPC && userdataA.equals("NPCollision")) {
            ((AbstractNPC) userdataB).collision = false;
        }
    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {}

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {}
}
