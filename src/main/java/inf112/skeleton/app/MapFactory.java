package inf112.skeleton.app;

import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.objects.PolygonMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.utils.Array;

import java.util.ArrayList;


public class MapFactory {
    /**
     * Creates a TiledMap based on a level, adds collision and sensor layers, and returns a Renderer based on that.
     * @param level path to the level
     * @param world the world fixtures originating from the level should be placed in
     * @param game the game instance
     * @return Renderer which can be used to render the level
     */
    public static OrthogonalTiledMapRenderer createLevel(String level, World world, GameSetup game) {
        TiledMap tiledMap = new TmxMapLoader().load(level);
        fixtureCreation(world, tiledMap, "Collision", false);

        String[] sensorLayers = {
                "NPCollision", "RoomExit", "GameOver", "Transport", "Shuttle",
                "LeaveCave", "LeaveCave2", "EnterCave", "EnterCave2", "Item"
        };
        for (String sensorLayer : sensorLayers) {
            fixtureCreation(world, tiledMap, sensorLayer, true);
        }
        return new OrthogonalTiledMapRenderer(tiledMap, 1f, game.batch);
    }

    private static void fixtureCreation(World world, TiledMap tiledMap, String layer, boolean isSensor) {
        if (tiledMap.getLayers().get(layer) == null) {
            return;
        }
        MapObjects collisionObjects = tiledMap.getLayers().get(layer).getObjects();
        for (MapObject mapObject : collisionObjects) {
            if (mapObject instanceof PolygonMapObject) {
                BodyDef bodyDef = new BodyDef();
                bodyDef.type = BodyDef.BodyType.StaticBody;
                Body body = world.createBody(bodyDef);
                float[] vertices = ((PolygonMapObject) mapObject).getPolygon().getTransformedVertices();
                Vector2[] worldVertices = new Vector2[vertices.length / 2];
                for (int i = 0; i < vertices.length / 2; i++) {
                    Vector2 current = new Vector2(
                            vertices[i * 2] / Constants.PPM,
                            vertices[i * 2 + 1] / Constants.PPM
                    );
                    worldVertices[i] = current;
                }
                PolygonShape shape = new PolygonShape();
                shape.set(worldVertices);
                FixtureDef fixtureDef = new FixtureDef();
                fixtureDef.shape = shape;
                fixtureDef.isSensor = isSensor;
                Fixture fixture = body.createFixture(fixtureDef);
                fixture.setUserData(layer);
                shape.dispose();
            }
        }
    }

    /**
     * Gets the x and y coordinates of a spawn point on a level.
     * @param level path of the level
     * @param spawnIndex the spawn the coordinates should point to
     * @return the coordinates of the spawn
     */
    public static float[] getPlayerSpawn(String level, int spawnIndex) {
        TiledMap tiledMap = new TmxMapLoader().load(level);
        MapObject playerSpawn = tiledMap.getLayers().get("PlayerSpawn").getObjects().get(spawnIndex);
        float[] vertices = ((PolygonMapObject) playerSpawn).getPolygon().getTransformedVertices();
        float[] spawn = new float[2];
        spawn[0] = vertices[0] / Constants.PPM;
        spawn[1] = vertices[1] / Constants.PPM;
        return spawn;
    }

    /**
     * Gets all spawn point of a type of NPC in a level.
     * @param level path to the level
     * @param NPCSpawn the type of NPC
     * @return List of Vector2 with x and y coordinates of the NPC spawns
     */
    public static ArrayList<Vector2> getNPCSpawns(String level, String NPCSpawn) {
        ArrayList<Vector2> spawns = new ArrayList<>();
        TiledMap tiledMap = new TmxMapLoader().load(level);
        MapObjects spawnObjects = tiledMap.getLayers().get(NPCSpawn).getObjects();
        for (MapObject spawn : spawnObjects) {
            float[] vertices = ((PolygonMapObject) spawn).getPolygon().getTransformedVertices();
            spawns.add(new Vector2(vertices[0] / Constants.PPM, vertices[1] / Constants.PPM));
        }
        return spawns;
    }

    /**
     * Removes a fixture of a certain type from the world.
     * @param world the world which contains the fixtures
     * @param type the userData of the fixtures to be removed
     */
    public static void removeFixtures(World world, String type) {
        Array<Body> bodies = new Array<>();
        world.getBodies(bodies);
        for (Body body : bodies) {
            for (Fixture fixture : body.getFixtureList()) {
                if (fixture.getUserData().equals(type)) {
                    body.destroyFixture(fixture);
                }
            }
        }
    }

}
