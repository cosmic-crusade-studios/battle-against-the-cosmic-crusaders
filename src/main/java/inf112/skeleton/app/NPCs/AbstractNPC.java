package inf112.skeleton.app.NPCs;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.TimeUtils;
import inf112.skeleton.app.Constants;
import inf112.skeleton.app.HitboxFactory;

public abstract class AbstractNPC {

    protected float x;
    protected float y;
    protected float speed;
    private final TextureRegion[][] textures;
    private final TextureRegion texture;
    protected Body body;
    private final long startTime;
    public boolean collision = false;
    private final World world;
    public boolean killed = false;
    public boolean despawned = false;
    private long deathTime = 0;

    /**
     * Creates a movable NPC by initializing a hitbox and a texture
     * @param x starting x-coordinate of the NPC
     * @param y starting y-coordinate of the NPC
     * @param world world where the NPC exists
     * @param width width of the NPC
     * @param height height of the NPC
     * @param texturePath path to the texture of the NPC
     * @param speed movement speed of the NPC
     */
    protected AbstractNPC(float x, float y, World world, int width, int height, String texturePath, float speed) {
        body = HitboxFactory.createHitbox(x, y, world, this, width, height);
        this.world = world;
        Texture sheet = new Texture(texturePath);
        this.textures = TextureRegion.split(
                sheet,
                sheet.getWidth() / 4,
                sheet.getHeight() / 6
        );

        startTime = TimeUtils.millis();
        texture = textures[0][0];
        this.speed = speed;
    }

    /**
     * Updates the state of the npc:
     * Turns it around in case of a collision.
     * Updates its velocity to a speed set through calls to movement functions.
     * Updates its current position.
     * Handles the process getting it ready for removal from the map when killed.
     */
    public void update() {
        if (x == body.getPosition().x * Constants.PPM) {
            turnAround();
        }
        body.setLinearVelocity(speed, body.getLinearVelocity().y);
        x = body.getPosition().x * Constants.PPM;
        y = body.getPosition().y * Constants.PPM;
        if (killed && deathTime == 0) {
            deathTime = TimeUtils.millis();
            speed = 0;
        }
        if (TimeUtils.timeSinceMillis(deathTime) > 1000 && deathTime != 0) {
            despawned = true;
        }
    }

    /**
     * Removes the npc from the game
     */
    public void despawn() {
        world.destroyBody(body);
    }

    /**
     * Gets the texture that is supposed to be drawn at the NPCs location.
     * Dependent on whether it is killed or which direction it is walking.
     * Different textures are still returned even if that state is the same, to make an animation.
     * @return Texture of the NPC, based on what it is doing
     */
    public TextureRegion getTexture() {
        if (killed) {
            return textures[5][getDeathFrame()];
        }
        if (body.getLinearVelocity().x > 0) {
            return textures[1][getFrame()];
        }
        if (body.getLinearVelocity().x < 0) {
            return textures[0][getFrame()];
        }
        return texture;
    }

    /**
     * Gets the location of where an NPC should be drawn, dependent on its hitbox.
     * @return NPC texture location
     */
    public abstract Vector2 getTextureCoords();

    protected int getFrame() {
        long seconds = TimeUtils.timeSinceMillis(startTime);
        return (int) (seconds / 150) % 4;
    }

    protected int getDeathFrame() {
        long seconds = TimeUtils.timeSinceMillis(deathTime);
        if (seconds < 250) {
            return 0;
        } else if (seconds < 500) {
            return 1;
        } else if (seconds < 750) {
            return 2;
        } else {
            return 3;
        }
    }

    /**
     * @return x-coordinate of the NPC
     */
    public float getX() {
        return x;
    }

    /**
     * @return y-coordinate of the NPC
     */
    public float getY() {
        return y;
    }

    /**
     * Reverses the direction of movement by negating the speed.
     */
    public void turnAround() {
        speed = speed * -1;
    }

}
