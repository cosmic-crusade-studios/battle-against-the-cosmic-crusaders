package inf112.skeleton.app.NPCs;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import inf112.skeleton.app.Constants;

public class MagmaAnt extends AbstractHostileNPC {

    /**
     * Creates a movable MagmaAnt by constructing an AbstractHostileNPC with fixed dimensions, textures and speed.
     * @param x starting x-coordinate of the MagmaAnt
     * @param y starting y-coordinate of the MagmaAnt
     * @param world world where the MagmaAnt exists
     */
    public MagmaAnt(float x, float y, World world) {
        super(x, y, world, 2, 1, "src/assets/graphics/Mobs/MagmaAnt.png", 1.0f);
    }

    /**
     * {@inheritDoc}
     * The MagmaAnt's hitbox is 2*1.
     */
    @Override
    public Vector2 getTextureCoords() {
        return new Vector2(x - Constants.TILEWIDTH, y - Constants.TILEHEIGHT / 2f);
    }

}
