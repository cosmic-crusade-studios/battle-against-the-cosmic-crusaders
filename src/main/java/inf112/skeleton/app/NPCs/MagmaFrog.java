package inf112.skeleton.app.NPCs;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import inf112.skeleton.app.Constants;

public class MagmaFrog extends AbstractNPC {

    /**
     * Creates a movable MagmaFrog by constructing an AbstractNPC with fixed dimensions, textures and speed.
     * @param x starting x-coordinate of the MagmaFrog
     * @param y starting y-coordinate of the MagmaFrog
     * @param world world where the MagmaFrog exists
     */
    public MagmaFrog(float x, float y, World world) {
        super(x, y, world, 1, 1, "src/assets/graphics/Mobs/MagmaFrog.png", 0.5f);
    }

    /**
     * {@inheritDoc}
     * The MagmaFrog's hitbox is 1*1.
     */
    @Override
    public Vector2 getTextureCoords() {
        return new Vector2(x - Constants.TILEWIDTH / 2f, y - Constants.TILEHEIGHT / 2f);
    }

}
