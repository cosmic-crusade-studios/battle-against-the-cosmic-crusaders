package inf112.skeleton.app.NPCs;

import com.badlogic.gdx.physics.box2d.World;
import inf112.skeleton.app.Constants;

public abstract class AbstractHostileNPC extends AbstractNPC {

    /**
     * Creates a movable HostileNPC by initializing a hitbox and a texture.
     * It differs from a normal NPC because it has an attack() method.
     * @param x starting x-coordinate of the NPC
     * @param y starting y-coordinate of the NPC
     * @param world world where the NPC exists
     * @param width width of the NPC
     * @param height height of the NPC
     * @param texturePath path to the texture of the NPC
     * @param speed movement speed of the NPC
     */
    protected AbstractHostileNPC(
            float x, float y, World world, int width, int height, String texturePath, float speed
    ) {
        super(x, y, world, width, height, texturePath, speed);
    }

    /**
     * Makes a hostile npc walk towards the player.
     * Since contact between a hostile npc and the player will kill the player
     * (except for jumping on the head of the npc), this can be considered an attack.
     *
     * @param playerX the player the npc will walk towards
     */
    public void attack(float playerX) {
        if (!collision) {
            speed = playerX < x ? -Math.abs(speed): Math.abs(speed);
        }
        body.setLinearVelocity(speed, body.getLinearVelocity().y);
        x = body.getPosition().x * Constants.PPM;
        y = body.getPosition().y * Constants.PPM;
    }

}
