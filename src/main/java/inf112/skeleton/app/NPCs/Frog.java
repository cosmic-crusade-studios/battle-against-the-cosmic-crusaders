package inf112.skeleton.app.NPCs;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import inf112.skeleton.app.Constants;

public class Frog extends AbstractNPC {

    /**
     * Creates a movable Frog by constructing an AbstractNPC with fixed dimensions, textures and speed.
     * @param x starting x-coordinate of the Frog
     * @param y starting y-coordinate of the Frog
     * @param world world where the Frog exists
     */
    public Frog(float x, float y, World world) {
        super(x, y, world, 1, 1, "src/assets/graphics/Mobs/Frog.png", 0.5f);
    }

    /**
     * {@inheritDoc}
     * The Frog's hitbox is 1*1.
     */
    @Override
    public Vector2 getTextureCoords() {
        return new Vector2(x - Constants.TILEWIDTH / 2f, y - Constants.TILEHEIGHT / 2f);
    }
}
