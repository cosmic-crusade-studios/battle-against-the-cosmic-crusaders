package inf112.skeleton.app.Levels;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import inf112.skeleton.app.*;
import inf112.skeleton.app.InputHandlers.AbstractInputHandler;
import inf112.skeleton.app.Player.AbstractPlayer;
import inf112.skeleton.app.Screens.GameOverScreen;
import inf112.skeleton.app.Screens.LevelSelectionScreen;
import inf112.skeleton.app.Screens.RoomSelectionScreen;
import org.lwjgl.opengl.GL20;

public abstract class Level extends ScreenAdapter {
    GameSetup game;
    protected OrthographicCamera camera;
    protected World world;
    protected OrthogonalTiledMapRenderer levelRenderer;
    protected AbstractPlayer player;
    protected ExtendViewport extendViewport;

    public String nextLevel;
    protected ColliderContactListener colliderListener;
    protected AbstractInputHandler inputHandler;


    /**
     * Creates a new level of the game.
     *
     * @param game instance of the game
     * @param map path of the level
     * @param world the world for the level
     */
    public Level(GameSetup game, String map, World world) {
        this.game = game;
        this.world = world;
        this.levelRenderer = MapFactory.createLevel(map, world, game);

        this.camera = new OrthographicCamera(
                50 * Constants.TILEHEIGHT,
                50 * Constants.TILEHEIGHT
        );
        this.extendViewport = new ExtendViewport(this.camera.viewportWidth, this.camera.viewportHeight, this.camera);

        colliderListener = new ColliderContactListener(map);
        this.world.setContactListener(colliderListener);
    }


    /**
     * Renders the game at each frame by updating the camera and the player position.
     * @param delta time since last render
     */
    @Override
    public void render(float delta) {
        world.step((1f / Gdx.graphics.getFramesPerSecond())*2.4f, 6, 2);
        camera.position.x = player.getX();
        camera.position.y = player.getY();
        camera.update();
        game.batch.setProjectionMatrix(extendViewport.getCamera().combined);
        levelRenderer.setView(camera);
        inputHandler.handleInput();
        player.update();

        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        if (colliderListener.isRoomChange()) {
            changeRoom();
        }

    }

    private void changeRoom() {
        colliderListener.setRoomChange(false);
        nextLevel = colliderListener.getNextLevel();
        switch (nextLevel) {
            case "RoomSelection" -> {
                dispose();
                game.setScreen(new RoomSelectionScreen(game));
            }
            case "Platform" -> {
                dispose();
                game.setScreen(new LevelSelectionScreen(game));
            }
            case "GameOver" -> {
                dispose();
                game.setScreen(new GameOverScreen(game));
            }
            case "src/assets/Level2/Level2Part2.tmx" -> {
                dispose();
                String[] parallaxesPaths = new String[4];
                parallaxesPaths[0] = "src/assets/Level2/CaveSky.png";
                parallaxesPaths[1] = "src/assets/Level2/CaveBack.png";
                parallaxesPaths[2] = "src/assets/Level2/CaveMiddle.png";
                parallaxesPaths[3] = "src/assets/Level2/CaveFront.png";


                float[][] xyFactors = {
                        {0.05f, 0.05f},
                        {0.2f, -0.2f},
                        {0.55f, -0.55f},
                        {0.85f, -0.85f}
                };

                int[] spawnPoints = {-150, -150, -150, -150};


                game.setScreen(new PlatformLevel(
                        game, nextLevel, colliderListener.getSpawnIndex(),
                        parallaxesPaths, "src/assets/audio/soundtrack/Volcanic.wav",
                        xyFactors, spawnPoints, true, -12f
                ));
            }
            case "src/assets/Level2/Level2Part1.tmx" -> {
                dispose();
                String[] parallaxesPaths = new String[3];
                parallaxesPaths[0] = "src/assets/Level2/Sky.png";
                parallaxesPaths[1] = "src/assets/Level2/Back.png";
                parallaxesPaths[2] = "src/assets/Level2/Middle.png";
                float[][] xyFactors = {
                        {0.05f, 0f},
                        {0.1f, 0f},
                        {0.35f, -0.2f}
                };
                int[] spawnpoints = {-150, -150, 0};
                game.setScreen(new PlatformLevel(
                        game, nextLevel, colliderListener.getSpawnIndex(),
                        parallaxesPaths, "src/assets/audio/soundtrack/Volcanic1.wav",
                        xyFactors, spawnpoints, false, -12f
                ));
            }
            default -> {
                dispose();
                game.setScreen(new Spaceship(game, nextLevel, colliderListener.getSpawnIndex()));
            }
        }
    }

    /**
     * Resizes the screen by adjusting the camera
     * @param width new width of the screen
     * @param height new height of the screen
     */
    @Override
    public void resize(int width, int height) {
        camera.viewportWidth = width / 4f;
        camera.viewportHeight = height / 4f;
        camera.update();
    }

    /**
     * Removes all fixtures from non-player bodies in the world and destroys them.
     */
    @Override
    public void dispose() {
        Array<Body> bodies = new Array<>();
        world.getBodies(bodies);
        for (Body body : bodies) {
            for (Fixture fixture : body.getFixtureList()) {
                if (!fixture.getUserData().equals("Player")) {
                    body.destroyFixture(fixture);
                }
            }
        }
    }

    public ColliderContactListener getColliderListener() {
        return colliderListener;
    }
}
