package inf112.skeleton.app.Levels;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import inf112.skeleton.app.Constants;
import inf112.skeleton.app.GameSetup;
import inf112.skeleton.app.InputHandlers.PlatformInputHandler;
import inf112.skeleton.app.MapFactory;
import inf112.skeleton.app.NPCs.*;
import inf112.skeleton.app.Parallax;
import inf112.skeleton.app.Player.PlatformPlayer;

import java.util.ArrayList;

public class PlatformLevel extends Level {
    private final Parallax[] backgroundLayers;
    private final ArrayList<AbstractNPC> npcs;
    private final Music music;
    private final TextureRegion[][] jumpTextureSheet;
    private final ArrayList<AbstractNPC> despawnedNPCs = new ArrayList<>();

    /**
     * Creates a new level of the game
     *
     * @param game the GameSetup instance for the game
     * @param map the path to the Tiled map file for this level
     * @param parallaxesPaths an array of paths to the parallax backgrounds to use
     * @param musicPath the path to the background music file to use
     * @param xyFactors an array of factors to adjust the parallax background scrolling speeds
     * @param parallaxSpawnpoints an array of indices of parallax spawn points in the Tiled map
     * @param repeatY whether the parallax backgrounds should repeat in the Y direction
     * @param gravity the gravity in the level
     */
    public PlatformLevel(
            GameSetup game, String map, String[] parallaxesPaths,
            String musicPath, float[][] xyFactors, int[] parallaxSpawnpoints,
            boolean repeatY, float gravity
    ) {
        this(game, map, 0, parallaxesPaths, musicPath,xyFactors, parallaxSpawnpoints, repeatY, gravity);
    }

    /**
     * Creates a new level of the game
     *
     * @param game the GameSetup instance for the game
     * @param map the path to the Tiled map file for this level
     * @param spawnIndex the index of the player spawn point in level
     * @param parallaxesPaths an array of paths to the parallax backgrounds to use
     * @param musicPath the path to the background music file to use
     * @param xyFactors an array of factors to adjust the parallax background scrolling speeds
     * @param parallaxSpawnpoints an array of indices of parallax spawn points in the Tiled map
     * @param repeatY whether the parallax backgrounds should repeat in the Y direction
     * @param gravity the gravity in the level
     */
    public PlatformLevel(
            GameSetup game, String map, int spawnIndex,
            String[] parallaxesPaths, String musicPath, float[][] xyFactors,
            int[] parallaxSpawnpoints, boolean repeatY, float gravity
    ) {
        super(game, map, new World(new Vector2(0, gravity), false));
        float[] playerSpawn = MapFactory.getPlayerSpawn(map, spawnIndex);
        this.player = new PlatformPlayer(playerSpawn[0], playerSpawn[1], world);
        inputHandler = new PlatformInputHandler((PlatformPlayer) player);
        npcs = getNPCS(map);
        music = Gdx.audio.newMusic(Gdx.files.internal(musicPath));
        music.setVolume(0.5f);
        music.setLooping(true);
        music.play();

        this.backgroundLayers = new Parallax[parallaxesPaths.length];
        for (int i = 0; i< parallaxesPaths.length; i++){
            this.backgroundLayers[i] = new Parallax(
                    new Texture(parallaxesPaths[i]), xyFactors[i][0], xyFactors[i][1], camera, parallaxSpawnpoints[i], repeatY
            );
        }
        Texture jumpySheet = new Texture("src/assets/Jumpy.png");
        this.jumpTextureSheet = TextureRegion.split(
                jumpySheet, jumpySheet.getWidth()/4, jumpySheet.getHeight()/6
        );
    }

    private ArrayList<AbstractNPC> getNPCS(String map) {
        final ArrayList<AbstractNPC> npcs;
        npcs = new ArrayList<>();
        for (Vector2 coords : MapFactory.getNPCSpawns(map, "GoatSpawn")) {
            npcs.add(new Goat(coords.x, coords.y, world));
        }
        if (map.equals("src/assets/Level1/level1.tmx")) {
            for (Vector2 coords : MapFactory.getNPCSpawns(map, "AntSpawn")) {
                npcs.add(new Ant(coords.x, coords.y, world));
            }
            for (Vector2 coords : MapFactory.getNPCSpawns(map, "FrogSpawn")) {
                npcs.add(new Frog(coords.x, coords.y, world));
            }
        } else {
            for (Vector2 coords : MapFactory.getNPCSpawns(map, "MagmaAntSpawn")) {
                npcs.add(new MagmaAnt(coords.x, coords.y, world));
            }
            for (Vector2 coords : MapFactory.getNPCSpawns(map, "MagmaFrogSpawn")) {
                npcs.add(new MagmaFrog(coords.x, coords.y, world));
            }
        }
        return npcs;
    }

    /**
     * {@inheritDoc}
     * Additionally handles controlling NPCs and powerups, and draws the parallax background, player and NPCs.
     */
    @Override
    public void render(float delta) {
        super.render(delta);
        handleNPCs();
        handlePowerUps();
        game.batch.begin();
        for (Parallax layer : backgroundLayers) {
            layer.render(game.batch);
        }
        game.batch.draw(
                player.getPlayerTexture(),
                player.getX() - Constants.TILEWIDTH / 2f,
                player.getY() - Constants.TILEHEIGHT
        );
        for (AbstractNPC npc : npcs) {
            Vector2 textureCoords = npc.getTextureCoords();
            game.batch.draw(npc.getTexture(), textureCoords.x, textureCoords.y);
        }
        game.batch.end();
        levelRenderer.render();
    }

    private void handlePowerUps() {
        if (colliderListener.isItemAction()) {
            colliderListener.setItemAction(false);
            if (player instanceof PlatformPlayer) {
                ((PlatformPlayer) player).setJumpPower(7);
                player.setPlayerTextureSheet(jumpTextureSheet);
            }
            MapFactory.removeFixtures(world, "Item");
            levelRenderer.getMap().getLayers().get("ItemTile").setVisible(false);
        }
    }

    private void handleNPCs() {
        for (AbstractNPC npc : npcs) {
            if (npc.despawned) {
                npc.despawn();
                despawnedNPCs.add(npc);
            }
            if (
                    npc instanceof AbstractHostileNPC
                            && Math.abs(player.getX() - npc.getX()) < 3 * Constants.PPM && !npc.killed
            ) {
                ((AbstractHostileNPC) npc).attack(player.getX());
            } else {
                npc.update();
            }
        }
        for (AbstractNPC npc : despawnedNPCs) {
            npcs.remove(npc);
        }
        despawnedNPCs.clear();
    }

    /**
     * {@inheritDoc}
     * Additionally stops the music of the level.
     */
    @Override
    public void dispose() {
        super.dispose();
        music.dispose();
    }
}

