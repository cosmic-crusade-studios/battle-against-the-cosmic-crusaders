package inf112.skeleton.app.Levels;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import inf112.skeleton.app.*;
import inf112.skeleton.app.InputHandlers.SpaceshipInputHandler;
import inf112.skeleton.app.Player.SpaceshipPlayer;

public class Spaceship extends Level {

    /**
     * Creates a new level of the game.
     *
     * @param game the GameSetup instance for the game
     * @param map the path to the level
     * @param spawnIndex the index of the player spawn point in the level
     */
    public Spaceship(GameSetup game, String map, int spawnIndex) {
        super(game, map, new World(new Vector2(0, 0), false));
        float[] playerSpawn = MapFactory.getPlayerSpawn(map, spawnIndex);
        this.player = new SpaceshipPlayer(playerSpawn[0], playerSpawn[1], world);
        inputHandler = new SpaceshipInputHandler((SpaceshipPlayer) player);
    }

    /**
     * Creates a new level of the game.
     *
     * @param game the GameSetup instance for the game
     * @param map the path to the level
     */
    public Spaceship(GameSetup game, String map) {
        this(game, map, 0);
    }

    /**
     * {@inheritDoc}
     * Additionally draws the player.
     */
    @Override
    public void render(float delta) {
        super.render(delta);

        levelRenderer.render();
        game.batch.begin();
        game.batch.draw(
                player.getPlayerTexture(),
                player.getX() - Constants.TILEWIDTH / 2f,
                player.getY() - Constants.TILEHEIGHT
        );
        game.batch.end();
    }
}
