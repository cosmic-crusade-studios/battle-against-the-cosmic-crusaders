package inf112.skeleton.app;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Parallax {
    private final Texture texture;
    private final float yFactor;
    private final float xFactor;

    private final Camera camera;

    private final int spawnpoint;
    private final boolean repeatY;

    /**
     * Use a for loop on this with multiple images with different x and y factors to make them move with a parallaxing effect.
     *
     * @param texture the background image
     * @param xFactor the speed in the x dimension
     * @param yFactor the speed in the y dimension
     * @param camera camera
     * @param spawnpoint where on the screen the image will spawn
     * @param repeatY if the image should repeat in the y dimension.
     */
    public Parallax(Texture texture, float xFactor, float yFactor, Camera camera, int spawnpoint, boolean repeatY) {
        this.texture = texture;
        this.xFactor = xFactor;
        this.yFactor = yFactor;
        this.camera = camera;
        this.repeatY = repeatY;
        if (repeatY) this.texture.setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat);
        else this.texture.setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.ClampToEdge);
        this.spawnpoint = spawnpoint;
    }


    /**
     * takes care of the rendering on the screen
     */
    public void render(SpriteBatch batch) {
        TextureRegion region = new TextureRegion(texture);
        int xOffset = (int) (camera.position.x * xFactor);
        int yOffset = (int) (camera.position.y * yFactor);
        region.setRegionX(xOffset % texture.getWidth());
        if (repeatY) region.setRegionY(yOffset % texture.getHeight());
        else region.setRegionY(yOffset + texture.getHeight() / 2 + spawnpoint);
        region.setRegionWidth((int) camera.viewportWidth);
        region.setRegionHeight((int) camera.viewportHeight);
        batch.draw(
                region,
                camera.position.x - camera.viewportWidth / 2,
                camera.position.y - camera.viewportHeight / 2
        );
    }
}
