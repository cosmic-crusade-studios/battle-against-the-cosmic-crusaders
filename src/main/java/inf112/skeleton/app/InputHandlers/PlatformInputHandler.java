package inf112.skeleton.app.InputHandlers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import inf112.skeleton.app.Player.PlatformPlayer;

public class PlatformInputHandler extends AbstractInputHandler {

    /**
     * Handles inputs and binds them to PlatformPlayer methods.
     * @param player player that will perform actions when keystrokes are registered
     */
    public PlatformInputHandler(PlatformPlayer player) {
        super(player);
    }

    /**
     * {@inheritDoc}
     * Space and Left Shift is additionally handled at Platform Levels.
     * The event where no keys are pressed, and updating the players velocity is also handled here.
     */
    @Override
    public void handleInput() {
        player.standStill();
        super.handleInput();
        if (Gdx.input.isKeyPressed(Input.Keys.SPACE)) {
            ((PlatformPlayer) player).jump();
        }
        if (Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT)) {
            player.sprint();
        }
        player.updateVelocity();
    }
}
