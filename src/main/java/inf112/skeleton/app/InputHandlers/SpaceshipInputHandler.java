package inf112.skeleton.app.InputHandlers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import inf112.skeleton.app.Player.SpaceshipPlayer;

public class SpaceshipInputHandler extends AbstractInputHandler {

    /**
     * Handles inputs and binds them to SpaceshipPlayer methods.
     * @param player player that will perform actions when keystrokes are registered
     */
    public SpaceshipInputHandler(SpaceshipPlayer player) {
        super(player);
    }

    /**
     * {@inheritDoc}
     * S or Down, W or Up and Left Shift is additionally handled at Spaceship Levels.
     * The event where no keys are pressed, and updating the players velocity is also handled here.
     */
    @Override
    public void handleInput() {
        player.standStill();
        super.handleInput();
        if (Gdx.input.isKeyPressed(Input.Keys.S)||Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
            ((SpaceshipPlayer) player).moveDown();
        }
        if (Gdx.input.isKeyPressed(Input.Keys.W)||Gdx.input.isKeyPressed(Input.Keys.UP)) {
            ((SpaceshipPlayer) player).moveUp();
        }
        if (Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT)) {
            player.sprint();
        }
        player.updateVelocity();
    }
}
