package inf112.skeleton.app.InputHandlers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import inf112.skeleton.app.Player.AbstractPlayer;

public abstract class AbstractInputHandler {

    protected AbstractPlayer player;

    /**
     * Handles inputs and binds them to AbstractPlayer methods.
     * @param player player that will perform actions when keystrokes are registered
     */
    public AbstractInputHandler(AbstractPlayer player) {
        this.player = player;
    }

    /**
     * Handles keystrokes a player can press, by calling that players functions for movement.
     * Keystrokes handled:
     * A or Left Arrow, D or Right Arrow.
     */
    public void handleInput() {
        if (Gdx.input.isKeyPressed(Input.Keys.A) || Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
            player.moveLeft();
        }
        if (Gdx.input.isKeyPressed(Input.Keys.D)||Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
            player.moveRight();
        }
    }

}
