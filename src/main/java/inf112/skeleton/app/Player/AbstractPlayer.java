package inf112.skeleton.app.Player;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.utils.TimeUtils;
import inf112.skeleton.app.Constants;
import inf112.skeleton.app.HitboxFactory;

public abstract class AbstractPlayer {

    protected Body playerBody;
    protected float xVelocity;
    protected float speed = 2f;
    protected TextureRegion[][] playerTextureSheet;
    protected TextureRegion playerTexture;
    protected long startTime;
    protected float x;
    protected float y;

    /**
     * Creates a movable player by initializing a hitbox and a texture
     * @param x starting x-coordinate of the player
     * @param y starting y-coordinate of the player
     * @param world the world where the player exists
     */
    public AbstractPlayer(float x, float y, World world) {
        playerBody = HitboxFactory.createHitbox(x, y, world, "Player", 1, 2);

        Texture sheet = new Texture("src/assets/BossSpriteSheet.png");
        this.playerTextureSheet = TextureRegion.split(
                sheet,
                sheet.getWidth() / 4,
                sheet.getHeight() / 6
        );

        startTime = TimeUtils.millis();
        playerTexture = playerTextureSheet[3][1];
    }

    /**
     * Updates the coordinates of the player object based on the position of the player body.
     */
    public void update() {
        x = playerBody.getPosition().x * Constants.PPM;
        y = playerBody.getPosition().y * Constants.PPM;
    }

    /**
     * @return x-coordinate of the player
     */
    public float getX() {
        return x;
    }

    /**
     * @return y-coordinate of the player
     */
    public float getY() {
        return y;
    }

    /**
     * Sets a new texture sheet for the player, making it appear different.
     * @param textures new texture sheet
     */
    public void setPlayerTextureSheet(TextureRegion[][] textures) {
        this.playerTextureSheet = textures;
    }

    /**
     * @return velocity in x-direction for the player
     */
    public float getxVelocity() {
        return xVelocity;
    }

    /**
     * Sets a new position for the player.
     * @param x x-coordinate of the players new position
     * @param y y-coordinate of the players new position
     */
    public void setPosition(float x, float y){
        playerBody.setTransform(x, y, 0);
    }

    /**
     * Gets the texture that is supposed to be drawn at the location of the player.
     * @return Texture of the player
     */
    public abstract TextureRegion getPlayerTexture();

    protected int getFrame() {
        long seconds = TimeUtils.timeSinceMillis(startTime);
        return (int) (seconds / 150) % 4;
    }

    /**
     * Sets the horizontal velocity to move the player to the left at its defined speed.
     */
    public void moveLeft() {
        xVelocity = -speed;
    }

    /**
     * Sets the horizontal velocity to move the player to the right at its defined speed.
     */
    public void moveRight() {
        xVelocity = speed;
    }

    /**
     * Sets the horizontal velocity so the player does not move horizontally.
     */
    public void standStill() {
        xVelocity = 0;
    }

    /**
     * Increases the horizontal velocity to make the player move faster horizontally.
     */
    public void sprint() {
        xVelocity *= 2;
    }

    /**
     * Uses the current horizontal velocity to move the player at the right speed.
     */
    public void updateVelocity() {
        playerBody.setLinearVelocity(xVelocity, playerBody.getLinearVelocity().y);
    }
}
