package inf112.skeleton.app.Player;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.World;

public class SpaceshipPlayer extends AbstractPlayer {
    private float yVelocity;

    /**
     * Creates a movable player by initializing a hitbox and a texture.
     * This player should be in a world seen from a top-down view with no gravity, and can move freely in all directions
     * @param x starting x-coordinate of the player
     * @param y starting y-coordinate of the player
     * @param world the world where the player exists
     */
    public SpaceshipPlayer(float x, float y, World world) {
        super(x, y, world);
        yVelocity = 0;
        speed = 0.75f;
    }

    /**
     * {@inheritDoc}
     * The texture returned is dependent on which horizontal and vertical direction the player is walking,
     * or if it is standing still. The same state will also return different textures to make an animation.
     */
    @Override
    public TextureRegion getPlayerTexture() {
        if (playerBody.getLinearVelocity().x > 0) {
            return playerTextureSheet[3][getFrame()];
        }
        if (playerBody.getLinearVelocity().x < 0) {
            return playerTextureSheet[2][getFrame()];
        }
        if (playerBody.getLinearVelocity().y > 0) {
            return playerTextureSheet[1][getFrame()];
        }
        if (playerBody.getLinearVelocity().y < 0) {
            return playerTextureSheet[0][getFrame()];
        }
        return playerTextureSheet[4][0];
    }

    /**
     * @return The velocity the players has in the vertical direction
     */
    public float getyVelocity() {
        return yVelocity;
    }

    /**
     * Sets the vertical velocity to move the player upwards based on its defined speed.
     */
    public void moveUp() {
        yVelocity = speed;
    }

    /**
     * Sets the vertical velocity to move the player downwards based on its defined speed.
     */
    public void moveDown() {
        yVelocity = -speed;
    }

    /**
     * {@inheritDoc}
     * Additionally sets the vertical velocity so the player does not move vertically.
     */
    @Override
    public void standStill() {
        super.standStill();
        yVelocity = 0;
    }

    /**
     * {@inheritDoc}
     * Additionally increases the vertical velocity to make the player move faster vertically.
     */
    @Override
    public void sprint() {
        super.sprint();
        yVelocity *= 2;
    }

    /**
     * Uses the current horizontal and vertical velocity to move the player at the right speed.
     */
    @Override
    public void updateVelocity() {
        playerBody.setLinearVelocity(xVelocity, yVelocity);
    }
}
