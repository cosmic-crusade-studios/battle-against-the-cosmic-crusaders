package inf112.skeleton.app.Player;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import inf112.skeleton.app.Constants;

public class PlatformPlayer extends AbstractPlayer {

    private int jumpPower;
    private boolean canJump = true;

    /**
     * Creates a movable player by initializing a hitbox and a texture.
     * This player should be in a world with gravity, and can jump.
     * @param x starting x-coordinate of the player
     * @param y starting y-coordinate of the player
     * @param world the world where the player exists
     */
    public PlatformPlayer(float x, float y, World world) {
        super(x, y, world);
        jumpPower = 5;
    }

    /**
     * {@inheritDoc}
     * Additionally handles when the player is allowed to jump.
     */
    @Override
    public void update() {
        super.update();
        if (y == playerBody.getPosition().y * Constants.PPM && playerBody.getLinearVelocity().y == 0) {
            canJump = true;
        }
    }

    /**
     * {@inheritDoc}
     * The texture returned is dependent on which horizontal direction the player is walking,
     * or if it is standing still. The same state will also return different textures to make an animation.
     */
    @Override
    public TextureRegion getPlayerTexture() {
        if (playerBody.getLinearVelocity().x > 0) {
            return playerTextureSheet[3][getFrame()];
        }
        if (playerBody.getLinearVelocity().x < 0) {
            return playerTextureSheet[2][getFrame()];
        }
        return playerTextureSheet[4][0];
    }

    /**
     * Applies a force upwards on the player, to simulate a jump.
     */
    public void jump() {
        if (canJump) {
            playerBody.applyLinearImpulse(new Vector2(0, jumpPower), playerBody.getPosition(), true);
            canJump = false;
        }
    }

    /**
     * Makes the player capable of jumping higher.
     * @param jumpPower how much force that should be applied during the jump
     */
    public void setJumpPower(int jumpPower) {
        this.jumpPower = jumpPower;
    }
}
