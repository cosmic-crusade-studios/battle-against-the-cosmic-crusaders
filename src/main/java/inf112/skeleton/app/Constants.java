package inf112.skeleton.app;

public final class Constants {
    public static final float PPM = 32;
    public static final int FPS = 60;
    public static final int TILEHEIGHT = 16;
    public static final int TILEWIDTH = 16;

}