package inf112.skeleton.app.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.ScreenUtils;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import inf112.skeleton.app.GameSetup;
import inf112.skeleton.app.Levels.PlatformLevel;
import inf112.skeleton.app.Levels.Spaceship;

public class MainMenuScreen implements Screen {

    private final GameSetup game;
    private final Viewport fitViewport;

    private final OrthographicCamera camera;

    private final Animation<TextureRegion> logoAnimation;

    private float stateTime;

    private final Stage stage;

    private final Texture background;


    /**
     * Screen for showing the main menu containing different buttons.
     *
     * @param game the GameSetup instance for the game
     */
    public MainMenuScreen(final GameSetup game) {
        this.game = game;
        background = new Texture("src/assets/MenuBackground.png");

        Sound buttonClick = Gdx.audio.newSound(Gdx.files.internal("src/assets/Audio/Sounds/ButtonClick.wav"));


        camera = new OrthographicCamera(980, 540);
        camera.position.set(camera.viewportWidth / 2, camera.viewportHeight / 2, 0);
        fitViewport = new FitViewport(camera.viewportWidth, camera.viewportHeight, camera);


        TextureRegion[][] textures = TextureRegion.split(new Texture(
                "src/assets/GameTitle.png"), 400, 192
        );
        TextureRegion[] texturesSheet = new TextureRegion[16];
        System.arraycopy(textures[0], 0, texturesSheet, 0, 16);
        logoAnimation = new Animation<>(0.1f, texturesSheet);

        // Image for the correct button, hovering and clicking
        TextureRegion[][] buttonTextures = TextureRegion.split(new Texture(
                "src/assets/ButtonSpriteSheet.png"), 96, 64
        );

        ImageButton.ImageButtonStyle newButtonStyle = new ImageButton.ImageButtonStyle();
        newButtonStyle.imageUp = new TextureRegionDrawable(buttonTextures[3][0]);
        newButtonStyle.imageOver = new TextureRegionDrawable(buttonTextures[3][1]);
        newButtonStyle.imageDown = new TextureRegionDrawable(buttonTextures[3][2]);

        ImageButton.ImageButtonStyle loadButtonStyle = new ImageButton.ImageButtonStyle();
        loadButtonStyle.imageUp = new TextureRegionDrawable(buttonTextures[2][0]);
        loadButtonStyle.imageOver = new TextureRegionDrawable(buttonTextures[2][1]);
        loadButtonStyle.imageDown = new TextureRegionDrawable(buttonTextures[2][2]);

        ImageButton.ImageButtonStyle helpButtonStyle = new ImageButton.ImageButtonStyle();
        helpButtonStyle.imageUp = new TextureRegionDrawable(buttonTextures[1][0]);
        helpButtonStyle.imageOver = new TextureRegionDrawable(buttonTextures[1][1]);
        helpButtonStyle.imageDown = new TextureRegionDrawable(buttonTextures[1][2]);

        ImageButton.ImageButtonStyle quitButtonStyle = new ImageButton.ImageButtonStyle();
        quitButtonStyle.imageUp = new TextureRegionDrawable(buttonTextures[0][0]);
        quitButtonStyle.imageOver = new TextureRegionDrawable(buttonTextures[0][1]);
        quitButtonStyle.imageDown = new TextureRegionDrawable(buttonTextures[0][2]);


        // Button positioning and clicking action
        ImageButton newButton = new ImageButton(newButtonStyle);
        float ybuttonPos = camera.viewportHeight / 2 - newButton.getHeight() / 0.75f;
        newButton.setPosition(camera.viewportWidth / 2 - texturesSheet[0].getRegionWidth() / 2f - 3, ybuttonPos);
        newButton.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                buttonClick.play();
                game.setScreen(new Spaceship(game,"src/assets/Spaceship/rooms/CargoBay.tmx"));
                dispose();
                return true;
            }
        });

        ImageButton loadButton = new ImageButton(loadButtonStyle);
        float loadXPos = newButton.getX() + 7 + newButton.getWidth();
        loadButton.setPosition(loadXPos, ybuttonPos);
        loadButton.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                game.setScreen(new CreditsScreen(game));
                dispose();
                return true;
            }
        });

        ImageButton helpButton = new ImageButton(helpButtonStyle);
        float helpXPos = loadButton.getX() + 7 + loadButton.getWidth();
        helpButton.setPosition(helpXPos, ybuttonPos);
        helpButton.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                game.setScreen(new HelpScreen(game));
                dispose();
                return true;
            }
        });

        ImageButton quitButton = new ImageButton(quitButtonStyle);
        quitButton.setPosition(helpButton.getX() + 7 + quitButton.getWidth(), ybuttonPos);
        quitButton.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                buttonClick.play();
                dispose();
                game.batch.dispose();
                Gdx.app.exit();
                return true;
            }
        });


        stage = new Stage(fitViewport, game.batch);
        Gdx.input.setInputProcessor(stage);

        // Adding the buttons to the stage
        stage.addActor(newButton);
        stage.addActor(loadButton);
        stage.addActor(helpButton);
        stage.addActor(quitButton);
    }


    @Override
    public void render(float delta) {
        ScreenUtils.clear(0, 0, 0, 1);
        stateTime += Gdx.graphics.getDeltaTime();

        TextureRegion currentFrame = logoAnimation.getKeyFrame(stateTime, true);

        camera.update();
        game.batch.setProjectionMatrix(fitViewport.getCamera().combined);
        game.batch.begin();
        game.batch.draw(background, 0, 0);
        game.batch.draw(
                currentFrame,
                camera.viewportWidth / 2 - currentFrame.getRegionWidth() / 2f,
                camera.viewportHeight / 2
        );
        game.batch.end();
        stage.act(stateTime);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void dispose() {
        stage.dispose();
    }

    @Override
    public void pause() {}

    @Override
    public void resume() {}

    @Override
    public void show() {}

    @Override
    public void hide() {}
}
