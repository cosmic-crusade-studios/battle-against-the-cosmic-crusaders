package inf112.skeleton.app.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.ScreenUtils;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import inf112.skeleton.app.ColliderContactListener;
import inf112.skeleton.app.GameSetup;
import inf112.skeleton.app.Levels.Spaceship;

public class RoomSelectionScreen implements Screen {
    private final GameSetup game;
    private final Viewport fitViewport;

    private final OrthographicCamera camera;

    private float stateTime;

    private final Stage stage;

    private final Texture background;


    /**
     * Screen for showing the screen where you can select which room to go to.
     *
     * @param game the GameSetup instance for the game
     */
    public RoomSelectionScreen(GameSetup game) {
        this.game = game;
        background = new Texture("src/assets/Spaceship/ButtonScreenBackground.png");

        //Sound buttonClick = Gdx.audio.newSound(Gdx.files.internal(""));
        camera = new OrthographicCamera(980, 540);
        camera.position.set(camera.viewportWidth / 2, camera.viewportHeight / 2, 0);
        fitViewport = new FitViewport(camera.viewportWidth, camera.viewportHeight, camera);

        stage = new Stage(fitViewport, game.batch);
        Gdx.input.setInputProcessor(stage);


        TextureRegion[][] buttonTextures = TextureRegion.split(new Texture(
                "src/assets/Spaceship/LevelSelectionButtons.png"), 160, 48
        );

        ImageButton.ImageButtonStyle bridgeButtonStyle = new ImageButton.ImageButtonStyle();
        bridgeButtonStyle.imageUp = new TextureRegionDrawable(buttonTextures[0][0]);
        bridgeButtonStyle.imageOver = new TextureRegionDrawable(buttonTextures[0][1]);
        bridgeButtonStyle.imageDown = new TextureRegionDrawable(buttonTextures[0][2]);

        ImageButton.ImageButtonStyle engineeringButtonStyle = new ImageButton.ImageButtonStyle();
        engineeringButtonStyle.imageUp = new TextureRegionDrawable(buttonTextures[1][0]);
        engineeringButtonStyle.imageOver = new TextureRegionDrawable(buttonTextures[1][1]);
        engineeringButtonStyle.imageDown = new TextureRegionDrawable(buttonTextures[1][2]);

        ImageButton.ImageButtonStyle cargoBayButtonStyle = new ImageButton.ImageButtonStyle();
        cargoBayButtonStyle.imageUp = new TextureRegionDrawable(buttonTextures[2][0]);
        cargoBayButtonStyle.imageOver = new TextureRegionDrawable(buttonTextures[2][1]);
        cargoBayButtonStyle.imageDown = new TextureRegionDrawable(buttonTextures[2][2]);

        ImageButton.ImageButtonStyle livingDeckButtonStyle = new ImageButton.ImageButtonStyle();
        livingDeckButtonStyle.imageUp = new TextureRegionDrawable(buttonTextures[3][0]);
        livingDeckButtonStyle.imageOver = new TextureRegionDrawable(buttonTextures[3][1]);
        livingDeckButtonStyle.imageDown = new TextureRegionDrawable(buttonTextures[3][2]);

        ImageButton.ImageButtonStyle medicalRoomButtonStyle = new ImageButton.ImageButtonStyle();
        medicalRoomButtonStyle.imageUp = new TextureRegionDrawable(buttonTextures[4][0]);
        medicalRoomButtonStyle.imageOver = new TextureRegionDrawable(buttonTextures[4][1]);
        medicalRoomButtonStyle.imageDown = new TextureRegionDrawable(buttonTextures[4][2]);


        ImageButton bridgeButton = new ImageButton(bridgeButtonStyle);
        float xPos = camera.viewportWidth / 2 - bridgeButton.getWidth() - 26;
        float yPos = camera.viewportHeight / 2 + 2;
        bridgeButton.setPosition(xPos, yPos);
        bridgeButton.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                ColliderContactListener.nextLevel = "src/assets/Spaceship/rooms/Bridge.tmx";
                dispose();
                game.setScreen(new Spaceship(game, "src/assets/Spaceship/rooms/TurboLiftUp.tmx"));
                return true;
            }
        });

        float xIncrement = bridgeButton.getWidth() + 64;
        float yIncrement = bridgeButton.getHeight() + 32;

        xPos += xIncrement;
        ImageButton engineeringButton = new ImageButton(engineeringButtonStyle);
        engineeringButton.setPosition(xPos, yPos);
        engineeringButton.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                ColliderContactListener.nextLevel = "src/assets/Spaceship/rooms/Engineering.tmx";
                dispose();
                game.setScreen(new Spaceship(game, "src/assets/Spaceship/rooms/TurboLiftUp.tmx"));
                return true;
            }
        });


        xPos -= xIncrement;
        yPos -= yIncrement;
        ImageButton cargoBayButton = new ImageButton(cargoBayButtonStyle);
        cargoBayButton.setPosition(xPos, yPos);
        cargoBayButton.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                ColliderContactListener.nextLevel = "src/assets/Spaceship/rooms/CargoBay.tmx";
                dispose();
                game.setScreen(new Spaceship(game, "src/assets/Spaceship/rooms/TurboLiftDown.tmx"));
                return true;
            }
        });


        xPos += xIncrement;
        ImageButton livingDeckButton = new ImageButton(livingDeckButtonStyle);
        livingDeckButton.setPosition(xPos, yPos);
        livingDeckButton.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                ColliderContactListener.nextLevel = "src/assets/Spaceship/rooms/LivingDeck.tmx";
                dispose();
                game.setScreen(new Spaceship(game, "src/assets/Spaceship/rooms/TurboLiftRight.tmx"));
                return true;
            }
        });


        xPos -= xIncrement;
        yPos -= yIncrement;
        ImageButton medicalRoomButton = new ImageButton(medicalRoomButtonStyle);
        medicalRoomButton.setPosition(xPos, yPos);
        medicalRoomButton.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                ColliderContactListener.nextLevel = "src/assets/Spaceship/rooms/MedicalRoom.tmx";
                dispose();
                game.setScreen(new Spaceship(game, "src/assets/Spaceship/rooms/TurboLiftUp.tmx"));
                return true;
            }
        });


        stage.addActor(bridgeButton);
        stage.addActor(engineeringButton);
        stage.addActor(cargoBayButton);
        stage.addActor(livingDeckButton);
        stage.addActor(medicalRoomButton);
    }


    @Override
    public void render(float delta) {
        ScreenUtils.clear(0, 0, 0, 1);
        stateTime += Gdx.graphics.getDeltaTime();

        camera.update();
        game.batch.setProjectionMatrix(fitViewport.getCamera().combined);
        game.batch.begin();
        game.batch.draw(background, 0, 0);
        game.batch.end();
        stage.act(stateTime);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void dispose() {
        stage.dispose();
    }

    @Override
    public void pause() {}

    @Override
    public void resume() {}

    @Override
    public void hide() {}

    @Override
    public void show() {}
}

