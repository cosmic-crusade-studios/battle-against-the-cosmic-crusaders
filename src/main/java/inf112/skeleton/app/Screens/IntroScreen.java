package inf112.skeleton.app.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.utils.ScreenUtils;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import inf112.skeleton.app.GameSetup;

public class IntroScreen implements Screen {


    final GameSetup game;
    private final Sprite logoSprite;
    private final Sprite presentsSprite;
    private final Music introMusic;
    private final ParticleEffect effect;
    private float logoOpacity;
    private float presentsOpacity;

    Camera camera;

    private final Viewport fitViewport;
    private boolean introDone;
    private float speed;
    private boolean logoAnimationDone;



    /**
     * Screen for showing the intro.
     *
     * @param game the GameSetup instance for the game
     */
    public IntroScreen(final GameSetup game) {
        this.game = game;

        introDone = false;
        speed = 0.175f;

        //intro music
        introMusic = Gdx.audio.newMusic(Gdx.files.internal("src/assets/audio/sounds/Intro1.wav"));
        introMusic.play();

        camera = new OrthographicCamera(980, 540);
        camera.position.set(camera.viewportWidth / 2, camera.viewportHeight / 2, 0);
        fitViewport = new FitViewport(camera.viewportWidth, camera.viewportHeight, camera);

        logoSprite = new Sprite(new Texture("src/assets/CosmicCrusadeStudiosLogo.png"));
        logoOpacity = 0.05f;
        logoSprite.setPosition(
                camera.viewportWidth / 2 - logoSprite.getWidth() / 2,
                camera.viewportHeight / 2 - logoSprite.getRegionHeight() / 2f
        );

        presentsSprite = new Sprite(new Texture("src/assets/presents.png"));
        presentsOpacity = 0.00f;
        presentsSprite.setPosition(
                camera.viewportWidth / 2 - presentsSprite.getWidth() / 2,
                camera.viewportHeight / 2 - presentsSprite.getRegionHeight() / 2f
        );

        effect = new ParticleEffect();
        effect.load(
                Gdx.files.internal("src/assets/graphics/effects/particles.p"),
                Gdx.files.internal("src/assets/graphics/effects/")
        );
        effect.getEmitters().first().setPosition(0,0);
        effect.start();
    }


    @Override
    public void render(float delta) {
        ScreenUtils.clear(0, 0, 0, 1);
        float animationTime = delta * speed;

        if (logoAnimationDone) {
            if (presentsOpacity + animationTime >= 1f) {
                presentsOpacity = 1f;
                speed *= -1f;
            } else if (presentsOpacity <= 0f) {
                presentsOpacity = 0f;
                if (!introMusic.isPlaying()) {
                    introDone = true;
                }
            } else {
                presentsOpacity += animationTime;
            }

        } else if (logoOpacity + animationTime >= 1f) {
            logoOpacity = 1f;
            speed *= -1f;
        } else if (logoOpacity <= 0f) {
            logoOpacity = 0f;
            presentsOpacity = 0.05f;
            logoAnimationDone = true;
            speed *= -1.3f; // to make the text opacity change faster
        } else {
            logoOpacity += animationTime;
        }


        camera.update();
        game.batch.setProjectionMatrix(fitViewport.getCamera().combined);

        effect.update(Gdx.graphics.getDeltaTime());

        game.batch.begin();

        effect.draw(game.batch);

        logoSprite.setColor(1, 1, 1, logoOpacity);

        logoSprite.draw(game.batch);

        presentsSprite.setColor(1, 1, 1, presentsOpacity);
        presentsSprite.draw(game.batch);

        game.batch.end();

        if (effect.isComplete()) effect.reset();

        if (Gdx.input.isTouched() || Gdx.input.isKeyPressed(Input.Keys.ANY_KEY) || introDone) {
            game.setScreen(new MainMenuScreen(game));
            dispose();
        }
    }

    @Override
    public void resize(int width, int height) {
        fitViewport.update(width, height, true);
    }

    @Override
    public void pause() {}

    @Override
    public void resume() {}

    @Override
    public void show() {}

    @Override
    public void hide() {}

    @Override
    public void dispose() {
        introMusic.dispose();
    }
}

