package inf112.skeleton.app.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.FitViewport;
import inf112.skeleton.app.GameSetup;

public class GameOverScreen implements Screen {

    final GameSetup game;
    private final OrthographicCamera camera;
    private final FitViewport fitViewport;
    private final Stage stage;

    private final Texture texture;
    private float stateTime;

    /**
     * Screen for showing that you failed.
     *
     * @param game the GameSetup instance for the game
     */
    public GameOverScreen(final GameSetup game) {
        this.game = game;
        this.texture = new Texture("src/assets/FlowersOnCasket.png");
        TextureRegion[][] buttonTextures = TextureRegion.split(
                new Texture("src/assets/ButtonSpriteSheet.png"), 96, 64
        );

        Sound buttonClick = Gdx.audio.newSound(Gdx.files.internal("src/assets/Audio/Sounds/ButtonClick.wav"));


        camera = new OrthographicCamera(980/2f, 540/2f);
        camera.position.set(camera.viewportWidth / 2, camera.viewportHeight / 2, 0);
        fitViewport = new FitViewport(camera.viewportWidth, camera.viewportHeight, camera);

        stage = new Stage(fitViewport, game.batch);
        Gdx.input.setInputProcessor(stage);


        ImageButton.ImageButtonStyle menuButtonStyle = new ImageButton.ImageButtonStyle();
        menuButtonStyle.imageUp = new TextureRegionDrawable(buttonTextures[4][0]);
        menuButtonStyle.imageOver = new TextureRegionDrawable(buttonTextures[4][1]);
        menuButtonStyle.imageDown = new TextureRegionDrawable(buttonTextures[4][2]);

        ImageButton menuButton = new ImageButton(menuButtonStyle);
        menuButton.setPosition(
                camera.viewportWidth/2 - menuButton.getWidth()/2,
                camera.viewportHeight/2-menuButton.getHeight()*2
        );
        menuButton.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                buttonClick.play();
                dispose();
                game.setScreen(new MainMenuScreen(game));
                return true;
            }
        });

        stage.addActor(menuButton);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.15f, 0.15f, 0.15f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stateTime += Gdx.graphics.getDeltaTime();

        camera.update();
        game.batch.setProjectionMatrix(fitViewport.getCamera().combined);
        game.batch.begin();
        game.batch.draw(texture, camera.viewportWidth / 2 - texture.getWidth() / 2f, camera.viewportHeight / 2);
        game.batch.end();
        stage.act(stateTime);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void dispose() {
        stage.dispose();
    }

    @Override
    public void pause() {}

    @Override
    public void resume() {}

    @Override
    public void show() {}

    @Override
    public void hide() {}
}
