package inf112.skeleton.app.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.ScreenUtils;
import com.badlogic.gdx.utils.viewport.FitViewport;
import inf112.skeleton.app.GameSetup;

public class CreditsScreen implements Screen {

    final GameSetup game;
    private float stateTime;

    private final Camera camera;

    private float y;

    private final BitmapFont font = new BitmapFont();

    private final FitViewport fitViewport;
    private final Stage stage;
    private final ImageButton menuButton;

    /**
     * Screen for showing the credits.
     *
     * @param game the GameSetup instance for the game
     */
    public CreditsScreen(final GameSetup game) {
        this.game = game;

        TextureRegion[][] buttonTextures = TextureRegion.split(
                new Texture("src/assets/ButtonSpriteSheet.png"), 96, 64
        );

        Sound buttonClick = Gdx.audio.newSound(Gdx.files.internal("src/assets/Audio/Sounds/ButtonClick.wav"));

        camera = new OrthographicCamera(980 / 2f, 540 / 2f);
        camera.position.set(camera.viewportWidth / 2, camera.viewportHeight / 2, 0);
        y = camera.viewportHeight / 2;
        fitViewport = new FitViewport(camera.viewportWidth, camera.viewportHeight, camera);

        stage = new Stage(fitViewport, game.batch);
        Gdx.input.setInputProcessor(stage);


        ImageButton.ImageButtonStyle menuButtonStyle = new ImageButton.ImageButtonStyle();
        menuButtonStyle.imageUp = new TextureRegionDrawable(buttonTextures[4][0]);
        menuButtonStyle.imageOver = new TextureRegionDrawable(buttonTextures[4][1]);
        menuButtonStyle.imageDown = new TextureRegionDrawable(buttonTextures[4][2]);

        menuButton = new ImageButton(menuButtonStyle);
        menuButton.setPosition(
                camera.viewportWidth / 2 - menuButton.getWidth() / 2,
                camera.viewportHeight / 2 - menuButton.getHeight() * 2
        );
        menuButton.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                buttonClick.play();
                dispose();
                game.setScreen(new MainMenuScreen(game));
                return true;
            }
        });

        stage.addActor(menuButton);
    }



    @Override
    public void render(float v) {
        ScreenUtils.clear(0, 0, 0, 1);
        stateTime += Gdx.graphics.getDeltaTime();

        y -= Gdx.graphics.getDeltaTime() * 25;

        camera.position.set(camera.viewportWidth / 2, y, 0);
        game.batch.setProjectionMatrix(camera.combined);
        camera.update();

        game.batch.begin();
        font.draw(game.batch, "We haven't implemented loading yet", camera.viewportWidth / 3, -10);
        font.draw(game.batch, "We have implemented credits though.", camera.viewportWidth / 3, -45);
        font.draw(game.batch, "Look at the credits.", camera.viewportWidth / 3, -65);
        font.draw(game.batch, "Credits:", camera.viewportWidth / 3, -100);
        font.draw(game.batch, "Graphics:", camera.viewportWidth / 3, -150);
        font.draw(game.batch, "Daniel Rolfsnes", camera.viewportWidth / 3, -175);
        font.draw(game.batch, "Audio:", camera.viewportWidth / 3, -250);
        font.draw(game.batch, "Daniel Rolfsnes", camera.viewportWidth / 3, -275);
        font.draw(game.batch, "Coding:", camera.viewportWidth / 3, -350);
        font.draw(game.batch, "Jonas Lund Qvigstad, Daniel Rolfsnes", camera.viewportWidth / 3, -375);
        font.draw(game.batch, "Testing:", camera.viewportWidth / 3, -450);
        font.draw(game.batch, "Magnus Severinsen", camera.viewportWidth / 3, -475);
        font.draw(game.batch, "Level Design:", camera.viewportWidth / 3, -550);
        font.draw(game.batch, "Benjamin Ramslien", camera.viewportWidth / 3, -575);
        font.draw(game.batch, "Thanks for playing :)", camera.viewportWidth / 3, -700);
        font.draw(game.batch, "You can leave this screen by clicking the Menu button", camera.viewportWidth / 5, -1500);
        font.draw(game.batch, "Please click the Button!", camera.viewportWidth / 3, -2500);
        font.draw(game.batch, "Click the Button!", camera.viewportWidth / 3, -3500);
        font.draw(game.batch, "CLICK THE BUTTON!", camera.viewportWidth / 3, -4500);
        font.draw(game.batch, "Press Alt+f4 and you will get a secret very OP item. Try it", camera.viewportWidth / 5, -5500);
        font.draw(game.batch, "Leave Now.", camera.viewportWidth / 3, -6500);
        font.draw(game.batch, "LEAVE!", camera.viewportWidth / 3, -7500);

        game.batch.end();
        menuButton.setPosition(0, -camera.viewportHeight / 2 + y);
        stage.act(stateTime);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        fitViewport.update(width, height, true);
    }

    @Override
    public void dispose() {
        stage.dispose();
    }

    @Override
    public void pause() {}

    @Override
    public void resume() {}

    @Override
    public void show() {}

    @Override
    public void hide() {}

}
