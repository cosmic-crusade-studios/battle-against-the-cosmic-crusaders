package inf112.skeleton.app.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.FitViewport;
import inf112.skeleton.app.GameSetup;
import inf112.skeleton.app.Levels.PlatformLevel;

public class LevelSelectionScreen implements Screen {

    final GameSetup game;
    private final OrthographicCamera camera;
    private final FitViewport fitViewport;
    private final Stage stage;

    private float stateTime;
    private final Texture background;


    /**
     * Screen for showing the screen where you can select levels.
     *
     * @param game the GameSetup instance for the game
     */
    public LevelSelectionScreen(final GameSetup game) {
        this.game = game;
        this.background = new Texture("src/assets/Spaceship/LevelSelection.png");
        TextureRegion[][] buttonTextures = TextureRegion.split(
                new Texture("src/assets/Planets.png"), 128, 128
        );

        Sound buttonClick = Gdx.audio.newSound(Gdx.files.internal("src/assets/Audio/Sounds/ButtonClick.wav"));


        camera = new OrthographicCamera(980, 540);
        camera.position.set(camera.viewportWidth / 2, camera.viewportHeight / 2, 0);
        fitViewport = new FitViewport(camera.viewportWidth, camera.viewportHeight, camera);

        stage = new Stage(fitViewport, game.batch);
        Gdx.input.setInputProcessor(stage);


        ImageButton.ImageButtonStyle level1ButtonStyle = new ImageButton.ImageButtonStyle();
        level1ButtonStyle.imageUp = new TextureRegionDrawable(buttonTextures[0][0]);
        level1ButtonStyle.imageOver = new TextureRegionDrawable(buttonTextures[0][1]);
        level1ButtonStyle.imageDown = new TextureRegionDrawable(buttonTextures[0][2]);

        ImageButton level1Button = new ImageButton(level1ButtonStyle);
        level1Button.setPosition(
                camera.viewportWidth / 3 - level1Button.getWidth() / 2,
                camera.viewportHeight / 2 - level1Button.getHeight()
        );
        level1Button.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                buttonClick.play();
                dispose();
                String map = "src/assets/Level1/level1.tmx";
                String[] parallaxesPaths = new String[4];
                parallaxesPaths[0] = "src/assets/graphics/backgrounds/Sky.png";
                parallaxesPaths[1] = "src/assets/graphics/backgrounds/Back.png";
                parallaxesPaths[2] = "src/assets/graphics/backgrounds/Middle.png";
                parallaxesPaths[3] = "src/assets/graphics/backgrounds/Front.png";
                float[][] xyFactors = {
                        {0f, 0f},
                        {0.1f, -0.4f},
                        {0.35f, -0.65f},
                        {0.6f, -0.8f}
                };

                int[] spawnpoints = {350,350,350,350};
                game.setScreen(new PlatformLevel(
                        game, map, parallaxesPaths,
                        "src/assets/audio/soundtrack/Mountain.wav",
                        xyFactors, spawnpoints, false, -9.81f
                ));
                return true;
            }
        });

        ImageButton.ImageButtonStyle level2ButtonStyle = new ImageButton.ImageButtonStyle();
        level2ButtonStyle.imageUp = new TextureRegionDrawable(buttonTextures[1][0]);
        level2ButtonStyle.imageOver = new TextureRegionDrawable(buttonTextures[1][1]);
        level2ButtonStyle.imageDown = new TextureRegionDrawable(buttonTextures[1][2]);

        ImageButton level2Button = new ImageButton(level2ButtonStyle);
        level2Button.setPosition(2 * camera.viewportWidth / 3 - level2Button.getWidth() / 2, camera.viewportHeight / 2 - level2Button.getHeight());
        level2Button.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                buttonClick.play();
                dispose();
                String map = "src/assets/Level2/Level2Part1.tmx";
                String[] parallaxesPaths = new String[3];
                parallaxesPaths[0] = "src/assets/Level2/Sky.png";
                parallaxesPaths[1] = "src/assets/Level2/Back.png";
                parallaxesPaths[2] = "src/assets/Level2/Middle.png";
                float[][] xyFactors = {
                        {0.05f, 0f},
                        {0.1f, 0f},
                        {0.35f, -0.3f}
                };

                int [] spawnpoints = {-150,-150,0};
                game.setScreen(new PlatformLevel(
                        game, map, parallaxesPaths,
                        "src/assets/audio/soundtrack/Volcanic1.wav",
                        xyFactors, spawnpoints, false, -12f
                ));
                return true;
            }
        });

        stage.addActor(level1Button);
        stage.addActor(level2Button);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.15f, 0.15f, 0.15f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stateTime += Gdx.graphics.getDeltaTime();

        camera.update();
        game.batch.setProjectionMatrix(fitViewport.getCamera().combined);
        game.batch.begin();
        game.batch.draw(background, 0, 0);
        game.batch.end();
        stage.act(stateTime);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void dispose() {
        stage.dispose();
    }

    @Override
    public void pause() {}

    @Override
    public void resume() {}

    @Override
    public void show() {}

    @Override
    public void hide() {}

}



