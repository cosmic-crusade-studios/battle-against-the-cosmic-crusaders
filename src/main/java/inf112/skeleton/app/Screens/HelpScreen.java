package inf112.skeleton.app.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.FitViewport;
import inf112.skeleton.app.GameSetup;

public class HelpScreen implements Screen {

    final GameSetup game;
    private final OrthographicCamera camera;
    private final FitViewport fitViewport;
    private final Stage stage;

    private float stateTime;

    private Texture texture;

    private int page;
    private final Texture page1;
    private final Texture page2;
    private final Texture page3;
    private final Texture page4;


    /**
     * Screen for showing the help you need.
     *
     * @param game the GameSetup instance for the game
     */
    public HelpScreen(GameSetup game) {
        this.game = game;
        this.page1 = new Texture(Gdx.files.internal("src/assets/HelpScreen/HelpPage1.png"));
        this.page2 = new Texture(Gdx.files.internal("src/assets/HelpScreen/HelpPage2.png"));
        this.page3 = new Texture(Gdx.files.internal("src/assets/HelpScreen/HelpPage3.png"));
        this.page4 = new Texture(Gdx.files.internal("src/assets/HelpScreen/HelpPage4.png"));
        page = 1;

        TextureRegion[][] buttonTextures = TextureRegion.split(
                new Texture("src/assets/ButtonSpriteSheet.png"), 96, 64
        );

        Sound buttonClick = Gdx.audio.newSound(Gdx.files.internal("src/assets/Audio/Sounds/ButtonClick.wav"));


        camera = new OrthographicCamera(980 / 2f, 540 / 2f);
        camera.position.set(camera.viewportWidth / 2, camera.viewportHeight / 2, 0);
        fitViewport = new FitViewport(camera.viewportWidth, camera.viewportHeight, camera);

        stage = new Stage(fitViewport, game.batch);
        Gdx.input.setInputProcessor(stage);


        ImageButton.ImageButtonStyle nextButtonStyle = new ImageButton.ImageButtonStyle();
        nextButtonStyle.imageUp = new TextureRegionDrawable(buttonTextures[5][0]);
        nextButtonStyle.imageOver = new TextureRegionDrawable(buttonTextures[5][1]);
        nextButtonStyle.imageDown = new TextureRegionDrawable(buttonTextures[5][2]);

        ImageButton nextButton = new ImageButton(nextButtonStyle);
        nextButton.setPosition(
                camera.viewportWidth - nextButton.getWidth(),
                camera.viewportHeight / 2 - nextButton.getHeight() * 2
        );
        nextButton.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (page < 4)page++;
                else {
                    dispose();
                    buttonClick.play();
                    game.setScreen(new MainMenuScreen(game));
                    return true;
                }
                return true;
            }
        });
        ImageButton.ImageButtonStyle backButtonStyle = new ImageButton.ImageButtonStyle();
        backButtonStyle.imageUp = new TextureRegionDrawable(buttonTextures[6][0]);
        backButtonStyle.imageOver = new TextureRegionDrawable(buttonTextures[6][1]);
        backButtonStyle.imageDown = new TextureRegionDrawable(buttonTextures[6][2]);

        ImageButton backButton = new ImageButton(backButtonStyle);
        backButton.setPosition(0, camera.viewportHeight / 2 - backButton.getHeight() * 2);
        backButton.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (page > 1) page--;
                else{
                    dispose();
                    buttonClick.play();
                    game.setScreen(new MainMenuScreen(game));
                    return true;
                }
                return true;
            }
        });

        stage.addActor(nextButton);
        stage.addActor(backButton);
    }

    @Override
    public void render(float v) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stateTime += Gdx.graphics.getDeltaTime();

        camera.update();
        game.batch.setProjectionMatrix(fitViewport.getCamera().combined);

        if (page == 1) {
            texture = page1;
        } else if (page == 2) {
            texture = page2;
        } else if (page == 3) {
            texture = page3;
        }else if (page == 4) {
            texture = page4;
        }

        game.batch.begin();
        game.batch.draw(texture, 0, 0);
        game.batch.end();
        stage.act(stateTime);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void dispose() {
        stage.dispose();
    }

    @Override
    public void pause() {}

    @Override
    public void resume() {}

    @Override
    public void show() {}

    @Override
    public void hide() {}
}
