# Oblig 2

## Prosjektrapport

- Så langt har rollene fungert veldig bra. I tillegg til rollene har vi god oversikt over hvilken arbeidsoppaver
som må gjøre gjennom Trello. Dette gjør at det er lettere å unngå og at to eller flere jobber med samme arbeidsoppgaver,
som også er med å forebygge andre problemer.
- Foreløpig har vi ikke hatt et stort behov for andre roller, men det kan jo selvfølgelig endre seg med tiden.
  * Programmeringsansvarlig: Har hovedansvar for selve programmering, men går også igjenom andre sin kode
  for å kvalitetssikre den.
  * Grafikk: Foreløpig ansvarlig for å lage all grafikken som er i bruk. Eksempler på dette er "byggeklossene" som blir 
  brukt til å lage de forskjellige levlene. I tillegg er det planlagt og lage parallakse bakgrunner.
  * Lyd: Alt av lyd/musikk som er brukt er foreløpig selvlagd.
  * Story: Hensikten med denne rollen er å lage en god bakgrunnshitorie til de forskjellige karakterene.
  Vi kommer ikke til å gjøre noe med det før vi har fullført MVP kravene.
  * Leveldesign: Ansvarlig for å lage de fleste levler som skal brukes i spillet.
  * Testansvarlig: Ansvarlig for å sette opp de fleste forskjellige testene. I tillegg må han kvalitetssikre de andre
  testene som har blitt lagd.
- Vi synes valgene vi har tatt er gode, trello hjelper oss med å holde det oversiktlig. Vi pleier og planlegge alt vi
  skal gjøre den neste uken ved hvert møte, her for alle si sin mening og alle er enig før vi avslutter møte.
- Gruppedynamikken er god, vi er sjeldent uenig om hva som skal gjøres, og dersom vi er uenig løses det relativit fort.
- Frem til nå har vi klart og samarbeid godt, trello tavlen gjør dette mye lettere. Alle deltar hver uke og gjør de
oppgavene de har blitt tildelt. Vi kan bli bedre på allokere tid til å skrive prosjektrapport osv... Vi er god til
å kommunisere og det skal vi ta med oss videre.
- Alle har bidratt til kodebasen, men noen mer enn andre. Noen grunner til det er fordi at foreløpig, er det noe 
arbeid skjer mer i bakgrunnen. Som for eksempel leveldesign, pixelkunsst, lyd, collisiondetection osv...
- Tre forbedringspunkter: Begynne på prosjektrapport tidligere, svare raskere på discord og bruke litt mer tid på
prosjektet.

### Møte 20.02.2023
- Alle er til stede (Daniel, Benjamin, Jonas og Magnus).
- Går over Trello for å verifisere det vi gjorde sist.
- Vi skal jobbe mot å fullføre MVP og rapportere inn på Trello brettet når vi finner noen problemer, eller nye ideer vi
ønsker å implementere

### Møte 27.02.2023
- Alle er til stede.
- Planlegger frem mot neste innlevering del B.
- Går igjennom det de forskjellige har gjort siden sist møte og ser på trello tavlen.
- Vi har oppdaget noen få bugs, når vi bruker fullskjerm modus.
- Vi har også funnet ut at spillet bruker mer og mer minne jo lengre tid det går. Må finne ut hvordan dette skal løses.
- Vi har begynt å bruke branches.

### Møte 06.03.2023
- Daniel, Jonas og Magnus er til stede, Benjamin møtte digitalt grunnet sykemeldelse.
- Gikk igjennom hva som er blitt gjort på level-fabrikken og diskuterte hvordan vi skal kode fysikk og kollisjoner.
- Vi fikset buggen som gjorde at spillet brukte mer minne jo lengre det gikk.
- Fordelte oppgaver, til neste gang skal kollisjon, fysikk og designet på første level være ferdig.

## Krav og spesifikasjon
* Vi har prioritert kravene vi skrev for MVP. Vi har kommet godt igang med de. Alle kravene utenom reise til planeter fungerer på ett grunleggende nivå. Vi legger ideer i Idé-tavlen på trello og blir sammen enige om hva vi skal implementere. Vi prioriterer det vi synes er viktigst for kvaliteten av produktet.
* Vi har ikke oversikt over brukerhistorier, akseptansekriterier og arbeidsoppgaver for det vi har tenkt å gjøre enda, men vi har en idé om hva vi skal gjøre.
* Vi blir enige i møtene vi har om hvem som skal gjøre hva. Hva som skal gjøres henter vi fra trello der alle kan skrive ideer til funksjonaliteter.
* Vi har ikke gjort noen justeringer på kravenene som er med i MVP, heller ikke i rekkefølgen.
* Vi har ikke møtt på noen bugs etter vi lagde level-fabrikken


## Produkt og kode
* Klassediagrammet heter `klassediagram_oblig2.png` og kan finnes i samme mappe som denne obligen.
* Hittil har vi ikke fått testene opp til det ønskede punktet. Dette er fordi vi har prioritert å få et fungerede system i forhold til de kravene som vi satt fra starten.
* Den dekningen vi hittil har tatt i betrakning er spilleren sine muligheter for bevegelse. Passe på at de ikke kan gå ut av banen først og framst. Videre ønsker vi å legge inn tester for generell bevegelse, mulige handlinger, kollisjon, skade, om banen er i riktig størrelse og annet. Det har i første omgang vært fokus på at vi skal ha et fungerede MVP, før testene ble satt fullt fokus på.
* Så langt kjører ikke testene som de skal, det tror vi er fordi libgdx er et stort rammeverk som må initialiseres, og det skjer ikke når vi har små tester. Har prøvd diverse løsninger uten å lykkes, planen er å få hjelp av en gruppeleder i neste gruppetime.