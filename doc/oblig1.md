# A2 - Konsept

"Open-World" på romskipet:
* Kan bevege seg rundt på romskipet (lignende Stardew Valley)
    * Spilleren har Inventory/Toolbar
* Spillfigur kan styres til venstre, høyre, oppover og nedover (ikke hopping)
* Romskip har forskjellige rom der spiller kan gjøre forskjellige handlinger på forskjellige områder i rommet. Noen rom
  er større en skjermen og skjermen vil scrolle
    * Transporter rom - transportere til planeter eller andre skip
    * cargo rom - lagringsplass samt shuttlepod
    * soverom - soving
    * kafeteria/sosialt rom - spising og sosialt
    * bridge - kontrollering av skip ++
    * turbolift - reise til forskjellige rom/områder på skipet
    * engineering - maskineriet til skipet
    * medical room?
    * science rom?
* Gjøre forskjellige quests for framgang i storyen
    * Fikse ting på skipet
    * Reise til forskjellige planeter med forskjellige egenskaper
        * Finne crew/gjenstander

Platformer på planeter:
* Spillfigur kan styres til høyre/venstre samt hoppe og falle.
* Todimensjonal verden:
    * Plattform/Bakke - horisontal flate spilleren kan stå og gå på
    * Vegg - vertikal flate som spilleren ikke kan gå gjennom
    * Spilleren beveger seg oppover ved å hoppe og nedover ved å falle
* Verden er større en skjermen og scroller (lignende terraria/mario)
* Fiender som beveger seg og er skadelige som kan stoppes ved hjelp av forskjellige gjenstander
* Kan finne gjenstander som gir spilleren forskjellige egenskaper
* Formålet med "level'ene" på planetene varierer mellom bl.a. å finne personer, å finne gjenstander.

# A3-A - Velg og tilpass en prosess for teamet

Vi møter hver gruppetime, vi har også en egen discord-server der vi kan diskutere, stille spørsmål og ha møter.

Vi har lagd en tavle på trello der vi har en seksjon for idéer, der vi kan skrive idéer vi har. Vi blir sammen enige om
hva vi skal legge til og flytter eventualt idé-kortet over til to-do seksjonen. Deretter flytter man kort fra to-do
til "In
development" når man har begynt på implementasjonen av en idé. Når man mener man er ferdig med implementasjonen
flytter man kortet over til "To be verified". Den av oss som har hovedansvaret for "Code Review" flytter kortet over til
"Completed" seksjonen og merger koden fra en branch sammen med main-branchen i git.

# A3-B - Få oversikt over forventet produkt

En kort beskrivelse av det overordnede målet for applikasjonen:<br>
Ett spill der spilleren spiller gjennom en fortelling. Spilleren har ett romskip som spilleren lever på, men den må
reise til forskjellige planeter og utføre forskjellige 'quests' for å progressere i fortellingen. På romskipet er det
litt mer fugleperspektiv (som i stardew valley) og ikke samme type todimensjonalitet som det er på planetene spilleren
reiser til (som i mario).

Krav til Minimum Viable Product (MVP):
* Vise spillebrett
* Vise spiller på spillebrett
* Flytte spiller (v.h.a. taster)
* Reise til planeter
* Startskjerm

En liste over brukerhistorier til systemet basert på MVP-kravene:
* Som spiller trenger jeg å se spillbrett for å kunne se hvor jeg er.
    * Akseptansekriterier: Spillbrettet vises, lagd ved hjelp av fabrikk.
    * Oppfyller: Vise spillbrett
* Som spiller trenger jeg fungerende kontroller av en sprite for å kunne bevege meg.
    * Akseptansekriterier: Spiller beveger seg på spillebrettet og riktig animasjon vises i forhold til retning
    * Oppfyller: Vise spiller på spillbrett, Flytte spiller
* Som utvikler trenger jeg kollisjonsoppdagelse for at spilleren kun kan bevege seg der det er ment.
    * Akseptansekriterier: Spiller blir stoppet av vegg eller andre objekter, den går ikke gjennom de
    * Oppfyller: Flytte spiller
* Som spiller trenger jeg en startskjerm med forskjellige knapper for å gjøre handlinger jeg vil.
    * Akseptansekriterier: Start knapp som starter ett nytt spill, Load game knapp?, Instruksjonsknapp som forteller
      hvordan jeg spiller, Avslutt knapp som avslutter spillet.
    * Oppfyller: Startskjerm
* Som spiller trenger jeg en planet å reise til for å spille spillet.
    * Akseptansekriterier: Annet miljø rundt spiller, animering av reising
    * Oppfyller: Reise til planeter

En prioritert liste over hvilke brukerhistorier dere vil ha med i første iterasjon:
* Som spiller trenger jeg å se spillbrett for å kunne se hvor jeg er.
* Som spiller trenger jeg fungerende kontroller av en sprite for å kunne bevege meg.

# A5 - Oppsumering

Så langt har planleggingen gått bra. Vi har alle vært på lik linje med hva vi ønsker å få til. I tillegg har har alle
blitt hørt i deres mening. Vi har mange ønsker om implementasjoner og hva vi ønsker å få ut av et prosjekt som dette.
Med de rollene som er definert kan vi fortløpende ha noen med overordnet ansvar for det de ønsker å strukturere i
tillegg til at alle kan prøve ut alt. 
Vi føler at oppgaven vi har valgt treffer godt med at den har en god del arbeid,
samt morsomme implementasjoner som vi ser fram til. Vi har et ønske om å få fram et MVP før neste innlevering, slik
at vi kan begynne for fullt med resten som vi ser mest fram til å jobbe med. Dette ser akkurat nå realistisk ut, men
vi får se om det blir vanskeligere med noen av implementasjonene enn andre.

# Roller

- Daniel - Lyd, Grafikk & Story
- Jonas - Programmeringsansvarlig
- Benjamin - Leveldesign & Story 
- Magnus - Testansvarlig
