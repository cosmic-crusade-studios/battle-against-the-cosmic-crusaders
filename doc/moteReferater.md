# Møte 20.02.2023

Alle er til stede (Daniel, Benjamin, Jonas og Magnus).

Går over Trello for å verifisere det vi gjorde sist.

Vi skal jobbe mot å fullføre MVP og rapportere inn på Trello brettet når vi finner noen problemer, eller nye ideer vi
ønsker å implementere


# Møte 27.02.2023

Alle er til stede.

Planlegger frem mot neste innlevering del B.

Går igjennom det de forskjellige har gjort siden sist møte og ser på trello tavlen.

Vi har oppdaget noen få bugs, når vi bruker fullskjerm modus.

Vi har også funnet ut at spillet bruker mer og mer minne jo lengre tid det går. Må finne ut hvordan dette skal løses.

Vi har begynt å bruke branches.


# Møte 06.03.2023

Daniel, Jonas og Magnus er til stede, Benjamin møtte digitalt grunnet sykemeldelse. 

Gikk igjennom hva som er blitt gjort på level-fabrikken og diskuterte hvordan vi skal kode fysikk og kollisjoner.

Vi fikset buggen som gjorde at spillet brukte mer minne jo lengre det gikk. 

Fordelte oppgaver, til neste gang skal kollisjon, fysikk og designet på første level være ferdig.


# Møte 13.03.2023

Alle er til stede

Vi har oppdaget noen visuelle bugs

Vi har lagt inn forskjellige rom i romskipet

Fordelt oppgaver relatert til Box2D (kollisjon/fysikk)


# Møte 20.03.2023

Daniel, Jonas og Magnus er til stede, Benjamin er syk.

Rom på skipet er innredet, startet å generalisere spiller-objekter, få kollisjon av forskjellige lag til å fungere korrekt

Jobbe videre mot å implementere manglende funksjonalitet for å få noe fungerende klart til presentasjon om 3 uker.

Videre skal vi jobbe med oppgaver for å få MVP fullført så vi etterhvert kan implementere NPC flere baner og mer.


# Møte 27.03.23

Alle er til stede.

Vi er veldig nærme å få til kollisjon, mye av tiden går til kollisjon.

Jobber med historie og karaktertrekk hos de forskjellige karakterene.

Planlegger hva som må gjøres frem mot neste innlevering. Alle gruppetimene frem mot innlevering går vekk grunnet påske.

Skal jobbe med å få til overgang mellom rom. Vi vil ha en pop-up med tekst hvor man kan velge hvilken rom man skal gå inn i.

# Møte 17.04.23

Alle er til stede.

Vi har fått kollisjon til å fungere slik vi ønsker!

Magnus setter fullt fokus på testing, Benjamin og Daniel jobber med lage level og implementere NPC og items.

Jonas har nettopp endret hele oppsettet på koden slik at vi har bedre struktur. Videre skal han jobbe med å implementere mobs/NPC

Vi har fullført overgang i mellom rommene.

# Møte 24.04.23

Alle er til stede

Vi har forbedret litt på koden. Vi har og begynt å lage level 2. Vi har også komt litt lenger med testing.

Vi har gått igjennom alle kravene for spillet og lagt inn det vi mangler på trello-tavlen vår.

Vi har fordelt oppgaver til de nevnte kravene. Vi skal komme lengre med npc's, mobs, items, tester og level2 