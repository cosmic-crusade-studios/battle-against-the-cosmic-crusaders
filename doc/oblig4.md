# Oblig 4


## Prosjektrapport
- Rollene har fungert veldig bra så lenge vi har holdt på og alle har trives med det arbeidet som de har hatt hovedansvar for.
- Vi har ikke endret noen av rollene fra forrige innlevering.
    * Programmeringsansvarlig: Har hovedansvar for selve programmering, men går også igjenom andre sin kode
      for å kvalitetssikre den.
    * Grafikk: Foreløpig ansvarlig for å lage all grafikken som er i bruk. Eksempler på dette er "byggeklossene" som blir
      brukt til å lage de forskjellige levlene. I tillegg er det planlagt og lage parallakse bakgrunner.
    * Lyd: Alt av lyd/musikk som er brukt er foreløpig selvlagd.
    * Leveldesign: Ansvarlig for å lage de fleste levler som skal brukes i spillet og spillteste.
    * Testansvarlig: Ansvarlig for å sette opp de fleste forskjellige testene. I tillegg må han kvalitetssikre de andre
      testene som har blitt lagd.
- Vi har fortsatt god gruppedynamikk og frem mot den siste obligen har vi hovedsakelig kommunisert via discord, 
og den fast gruppetimen vi har på mandager. Vi skulle ønske vi hadde litt bedre tid til å komme enda lenger på spillet, 
men vi fortsatt fornøyd med hvor langt vi har komt.
- Fra oblig3 til oblig4 har vi fått gjort veldig mye, og som nevnt tidligere skulle vi ønske vi hadde litt bedre tid,
men vi føler også at vi får levert et ferdig spill. Vi kommer til å fortsette med spillet i ettertid for å gjøre
oss ferdig.
- Tre forbedringspunkter: Bruke trello bedre og fokusere de viktigste elementene, allokere flere ressurser til 
testingen og ha bedre responstid når noen på gruppen stiller et spørsmål.

- Alt i alt vil vi si at prosjektet har gått veldig bra. Vi skulle ønske vi kom litt lengre på prosjektet, men vi innså
at vi hadde litt for store ambisjoner. Den største utfordringen vi hadde var collision. Det gjorde at vi stod stille
litt for lenge, men uten om det har prosjektet gått veldig bra og det har vært veldig lærerikt og givende. I og med at
vi lagde vårt eget spill synes vi at vi har vært kreative og alle i gruppen har vært veldig flinke på sitt ansvarsområde.
Om vi skulle startet prosjektet på nytt ville vi nok allokert mer arbeidskraft til programmerings og test-delen av prosjektet
i fra starten av, sånn at de store problemene hadde blitt løst tidligere. Deretter kunne man heller skiftet fokuset over
på leveldesign for å komme med et eksempel.

### Møte 17.04.23
- Alle er til stede.
- Vi har fått kollisjon til å fungere slik vi ønsker!
- Magnus setter fullt fokus på testing, Benjamin og Daniel jobber med lage level og implementere NPC og items.
- Jonas har nettopp endret hele oppsettet på koden slik at vi har bedre struktur. Videre skal han jobbe med å implementere mobs/NPC
- Vi har fullført overgang i mellom rommene.

### Møte 24.04.23
- Alle er til stede
- Vi har forbedret litt på koden. Vi har og begynt å lage level 2. Vi har også komt litt lenger med testing.
- Vi har gått igjennom alle kravene for spillet og lagt inn det vi mangler på trello-tavlen vår.
- Vi har fordelt oppgaver til de nevnte kravene. Vi skal komme lengre med npc's, mobs, items, tester og level2


## Krav og spesifikasjon

Vi har prioritert level 2, item og mobs i tillegg til noen småting. 
Vi har kommet forbi MVP, men har fortsatt flere idéer til hva vi kan legge til. Idéer/brukerhistorier/arbeidsoppgaver legger vi til i trello-tavlen vi har lagd.

## Produkt og kode

Klassediagrammet heter `klassediagram_oblig4.png` og kan finnes i samme mappe som denne obligen.

De fleste testfilene blir kjørt ved å kjøre AppTest. PlayerTests og NPCTests kan ikke kjøre på ved hjelp av dette fordi de ikke har en egen RunWith metode.
Akkurat nå har vi 95% class coverage, 77% method coverage og 78% line coverage.

