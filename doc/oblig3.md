# Oblig 3

## Prosjektrapport

- Rollene fungerer fortsatt greit, vi har insett at vi ikke har så god tid som vi skulle ønske og derfor er det noen
ting som må nedprioriteres. Et eksempel på noe som må nedprioriteres er storyen.
- Foreløpig har vi ikke hatt et stort behov for andre roller, men det kan jo selvfølgelig endre seg med tiden.
    * Programmeringsansvarlig: Har hovedansvar for selve programmering, men går også igjenom andre sin kode
      for å kvalitetssikre den.
    * Grafikk: Foreløpig ansvarlig for å lage all grafikken som er i bruk. Eksempler på dette er "byggeklossene" som blir
      brukt til å lage de forskjellige levlene. I tillegg er det planlagt og lage parallakse bakgrunner.
    * Lyd: Alt av lyd/musikk som er brukt er foreløpig selvlagd.
    * Leveldesign: Ansvarlig for å lage de fleste levler som skal brukes i spillet og spillteste.
    * Testansvarlig: Ansvarlig for å sette opp de fleste forskjellige testene. I tillegg må han kvalitetssikre de andre
      testene som har blitt lagd.
- Vi har god gruppedynamikk og vi diskuterer jevnt via discord og fysiske møter. Vi er som regel på bølgelengde og vi
er alle enig om hvordan vi ønsker at sluttproduktet skal se ut.
- Kommunikasjonen fungerer bra, den gjøres som regel via discord, men også gjennom fysiske møter en gang i uken. Alle
får sagt det de ønsker og alle blir hørt.
- Når vi ser tilbake på det som har blitt gjort siden forrige oblig er ikke det alt for mye. Grunnen til dette er at vi
har stått fast på kollisjon en god stund. Dette gjør at vi har blitt forsinket. Men vi har kommunisert og arbeidetet bra sammen.
Det skal sies at vi har komt en del lengre, men vi hadde sett for oss at vi hadde klart å få til mer siden forrige oblig.
- Alle har bidratt til kodebasen, men noen mer enn andre. Noen grunner til det er fordi at foreløpig, er det noe
  arbeid skjer mer i bakgrunnen. Som for eksempel leveldesign, pixelkunsst, lyd, collisiondetection osv...
- Tre forbedringspunkter: Bruke mer tid mot slutten av prosjektet, bruke trello bedre og fokusere de viktigste elementene
av spillet


### Møte 13.03.2023
- Alle er til stede
- Vi har oppdaget noen visuelle bugs
- Vi har lagt inn forskjellige rom i romskipet
- Fordelt oppgaver relatert til Box2D (kollisjon/fysikk)


### Møte 20.03.2023
- Daniel, Jonas og Magnus er til stede, Benjamin er syk.
- Rom på skipet er innredet, startet å generalisere spiller-objekter, få kollisjon av forskjellige lag til å fungere korrekt
- Jobbe videre mot å implementere manglende funksjonalitet for å få noe fungerende klart til presentasjon om 3 uker.
- Videre skal vi jobbe med oppgaver ofr å få MVP fullført så vi etterhvert kan implementere NPC flere baner og mer.


### Møte 27.03.23
- Alle er til stede.
- Vi er veldig nærme å få til kollisjon, mye av tiden går til kollisjon.
- Jobber med historie og karaktertrekk hos de forskjellige karakterene.
- Planlegger hva som må gjøres frem mot neste innlevering. Alle gruppetimene frem mot innlevering går vekk grunnet påske.
- Skal jobbe med å få til overgang mellom rom. Vi vil ha en pop-up med tekst hvor man kan velge hvilken rom man skal gå inn i.


## Krav og spesifikasjon
* Vi har prioritert kollisjon og har implementert ett brukande utkast av det. Lyd og grafikk ansvarlig har jobbet litt med lyd og grafikk. Vi har kommet forbi MVP'et vi lagde i begynnelsen av semesteret, men vi har fortsatt flere funksjonaliteter vi ønsker å implementere.
* Vi prioriterer det vi mener er det viktigste for å forbedre spillopplevelsen. Vi legger ideer i Idé-tavlen på trello og blir sammen enige om hva vi skal implementere.


## Produkt og kode
* Klassediagrammet heter `klassediagram_oblig3.png` og kan finnes i samme mappe som denne obligen.
* Vi har klart å få testene til å sjekke om funksjonaliteten rundt Box2d fungerer slik ønsket. Men vi har fremdeles problemer med å få testene som skal sjekke at forskjelle aspekter av spillet fungerer som planlagt. Skal spørre om det videre i på gruppetimene, og få et initielt testene til å fungere som ønsket. Har satt om for å teste forskjellige ting rundt blant annet player-objekt og level-funksjonalitet.
